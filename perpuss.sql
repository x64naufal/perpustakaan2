-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 09 Jun 2022 pada 18.08
-- Versi server: 10.4.21-MariaDB
-- Versi PHP: 8.0.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `perpuss`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(128) NOT NULL,
  `fullname` varchar(100) NOT NULL,
  `nama_admin` varchar(100) NOT NULL,
  `color` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id_admin`, `username`, `password`, `fullname`, `nama_admin`, `color`) VALUES
(34, 'admin123', '3332316e696d6461', 'Naufal V2', 'Naufal V2', 'dark'),
(35, 'adminv2', '32766e696d6461', 'Admin V2', 'Admin V2', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `anggota`
--

CREATE TABLE `anggota` (
  `id_anggota` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(128) NOT NULL,
  `fullname` varchar(100) NOT NULL,
  `nama_anggota` varchar(100) NOT NULL,
  `color` varchar(30) NOT NULL,
  `jk_anggota` char(1) DEFAULT NULL,
  `kelas_anggota` varchar(10) NOT NULL,
  `jurusan_anggota` varchar(2) DEFAULT NULL,
  `no_telp_anggota` varchar(13) DEFAULT NULL,
  `alamat_anggota` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `anggota`
--

INSERT INTO `anggota` (`id_anggota`, `username`, `password`, `fullname`, `nama_anggota`, `color`, `jk_anggota`, `kelas_anggota`, `jurusan_anggota`, `no_telp_anggota`, `alamat_anggota`) VALUES
(40, 'anggota123', '33323161746f67676e61', 'Anggota Tester', 'Anggota Tester', 'dark', 'L', 'XI RPL A', 'Re', '087815810106', 'Jln HKSN'),
(41, 'anggotav2', '33323173616775746570', 'Anggota V2', 'Anggota V2', 'dark', 'L', 'XI RPL A', 'Re', '087815810106', 'Jln HKSN'),
(42, 'admincy', '79636e696d6461', 'Naufal Elghani', 'Naufal Elghani', 'dark', 'L', 'XI RPL A', 'Re', '087815810106', 'Jln HKSN');

-- --------------------------------------------------------

--
-- Struktur dari tabel `announce`
--

CREATE TABLE `announce` (
  `announce_id` int(11) NOT NULL,
  `announce_user` varchar(30) NOT NULL,
  `announce_session` varchar(30) NOT NULL,
  `announce_date` varchar(30) NOT NULL,
  `announce_msg` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `announce`
--

INSERT INTO `announce` (`announce_id`, `announce_user`, `announce_session`, `announce_date`, `announce_msg`) VALUES
(1, 'Petugas', 'Petugas Naufal', 'December 13, 2021', 'Ini adalah pesan yang dibuat oleh petugas :)'),
(2, 'Petugas', 'Naufal Elghani', 'December 14, 2021', 'Pengumuman untuk semuanya, welcome to my website!\r\nPlease like and comment my website and free rating :)\r\n\r\nInstagram: naufalagler'),
(3, 'Petugas', 'Petugas Naufal', 'December 14, 2021', 'Pesan terbaru telah dimasukan kedalam pemberitahuan, silahkan lihat dan cek untuk melihat pesan yang baru saya update!\r\n\r\n'),
(4, 'Petugas', 'Petugas Naufal', 'December 13, 2021', 'Jangan lupa untuk selalu mengikuti perkembangan dan update an dari website ini ya teman teman :)\r\n\r\nTerimakasih semuanya!!'),
(5, 'Anggota', 'Naufal Anggota', 'December 15, 2021', 'Halo salam dari binjai, just testing for this messages! i work from home :)');

-- --------------------------------------------------------

--
-- Struktur dari tabel `buku`
--

CREATE TABLE `buku` (
  `id_buku` int(11) NOT NULL,
  `judul_buku` varchar(50) NOT NULL,
  `genre_buku` varchar(50) NOT NULL,
  `penulis_buku` varchar(50) NOT NULL,
  `penerbit_buku` varchar(50) NOT NULL,
  `tahun_penerbit` char(4) DEFAULT NULL,
  `stok_buku` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `buku`
--

INSERT INTO `buku` (`id_buku`, `judul_buku`, `genre_buku`, `penulis_buku`, `penerbit_buku`, `tahun_penerbit`, `stok_buku`) VALUES
(3, 'A Certain Magical Index', 'Seni', '-', '-', '-', 8),
(5, 'Mushoko Tensei: Jobless Reincarnation', 'Aksi', '-', '-', '-', 0),
(7, 'Mushoko Tensei: Jobless Reincarnation 2', 'Aksi', '-', '-', '-', 0),
(10, 'Mushoko Tensei: Jobless Reincarnation 3', 'Aksi', '-', '-', '-', 0),
(11, 'Mushoko Tensei: Jobless Reincarnation 4', 'Aksi', '-', '-', '-', 0),
(12, 'Mushoko Tensei: Jobless Reincarnation 5', 'Aksi', '-', '-', '-', 0),
(13, 'Mushoko Tensei: Jobless Reincarnation 6', 'Aksi', '-', '-', '-', 0),
(14, 'Mushoko Tensei: Jobless Reincarnation 7', 'Aksi', '-', '-', '-', 0),
(15, 'Mushoko Tensei: Jobless Reincarnation 8', 'Aksi', '-', '-', '-', 0),
(16, 'Mushoko Tensei: Jobless Reincarnation 9', 'Aksi', '-', '-', '-', 0),
(17, 'Mushoko Tensei: Jobless Reincarnation 10', 'Seni', '-', '-', '-', 9);

-- --------------------------------------------------------

--
-- Struktur dari tabel `livechat`
--

CREATE TABLE `livechat` (
  `chat_id` int(11) NOT NULL,
  `chat_user` varchar(30) NOT NULL,
  `chat_session` varchar(30) NOT NULL,
  `chat_date` varchar(30) NOT NULL,
  `chat_msg` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `livechat`
--

INSERT INTO `livechat` (`chat_id`, `chat_user`, `chat_session`, `chat_date`, `chat_msg`) VALUES
(1, 'petugas', 'Petugas Naufal', 'December 16, 2021', 'helloo'),
(2, 'petugas', 'Petugas Naufal', 'December 16, 2021', 'Ini adlah pesan percoban'),
(3, 'petugas', 'Petugas Naufal', 'December 16, 2021', 'Test\r\n\r\none'),
(5, 'admin', 'admincy', 'December 16, 2021', 'ADMIN WAS HEREEE'),
(7, 'anggota', 'Anggota Cy', 'December 16, 2021', 'Haloo apa ada orang disini?'),
(8, 'admin', 'admincy', 'December 16, 2021', 'Saya sebagai admin akan mencoba dalam menulis dan mengirim  pesan, apakah livechat ini masuk kedalam database atau menjadi pesan pending.\r\nAdapun diantara yang lainnya yang belum  saya sebutkan secara menyeluruh bisa tolong anda konfirmasikan kepada saya selaku admin untuk kejelasan info dan lainnya'),
(9, 'petugas', 'Petugas Malam', 'January 6, 2022', 'Test dimalam harii'),
(10, 'admin', 'Admin Malam 2', 'January 6, 2022', 'AHaloo malam'),
(11, 'anggota', 'Anggota Cy 2', 'January 6, 2022', 'anggota hadir!!'),
(12, 'admin', 'admincy', 'December 16, 2021', 'Saya sebagai admin akan mencoba dalam menulis dan mengirim  pesan, apakah livechat ini masuk kedalam database atau menjadi pesan pending.\r\nAdapun diantara yang lainnya yang belum  saya sebutkan secara menyeluruh bisa tolong anda konfirmasikan kepada saya selaku admin untuk kejelasan info dan lainnya'),
(13, 'anggota', 'Anggota Tester', 'May 25, 2022', 'Test pada hari ini saya tulis');

-- --------------------------------------------------------

--
-- Struktur dari tabel `peminjaman`
--

CREATE TABLE `peminjaman` (
  `id_peminjaman` int(11) NOT NULL,
  `tanggal_pinjam` date DEFAULT NULL,
  `tanggal_kembali` date DEFAULT NULL,
  `id_buku` int(11) DEFAULT NULL,
  `id_anggota` int(11) DEFAULT NULL,
  `id_petugas` int(11) DEFAULT NULL,
  `nama_petugas` varchar(30) NOT NULL,
  `status` varchar(30) NOT NULL,
  `sesi_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `peminjaman`
--

INSERT INTO `peminjaman` (`id_peminjaman`, `tanggal_pinjam`, `tanggal_kembali`, `id_buku`, `id_anggota`, `id_petugas`, `nama_petugas`, `status`, `sesi_id`) VALUES
(30, '2022-06-06', '2022-06-08', 3, 40, 4, 'Naufal V2 (admin)', 'terima', 0),
(32, '2022-06-06', '2022-06-14', 16, 40, 4, 'Naufal V2 (admin)', 'terima', 0),
(33, '2022-06-06', '2022-06-24', 3, 41, 4, 'Naufal V2 (admin)', 'terima', 0),
(38, '2022-06-09', '2022-06-10', 10, 41, 34, 'Petugas Naufal V2 (petugas)', 'terima', 0),
(41, '2022-06-09', '2022-06-17', 3, 0, NULL, 'Naufal V2 (admin)', 'tunda', 0),
(43, '2022-06-09', '2022-06-11', 3, 40, 34, 'Petugas Naufal V2 (petugas)', 'terima', 0),
(44, '2022-06-09', '2022-06-09', 3, 40, 34, 'Petugas Naufal V2 (petugas)', 'terima', 0),
(46, '2022-06-09', '2022-06-15', 10, 40, 0, '', 'tunda', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengembalian`
--

CREATE TABLE `pengembalian` (
  `id_pengembalian` int(11) NOT NULL,
  `tanggal_pengembalian` date NOT NULL,
  `denda` int(11) NOT NULL,
  `id_buku` int(11) NOT NULL,
  `id_anggota` int(11) NOT NULL,
  `id_petugas` int(11) NOT NULL,
  `nama_petugas` varchar(30) NOT NULL,
  `status` varchar(30) NOT NULL,
  `sesi_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pengembalian`
--

INSERT INTO `pengembalian` (`id_pengembalian`, `tanggal_pengembalian`, `denda`, `id_buku`, `id_anggota`, `id_petugas`, `nama_petugas`, `status`, `sesi_id`) VALUES
(47, '2022-06-09', 0, 3, 40, 4, 'Petugas Naufal V2 (petugas)', 'terima', 0),
(48, '2022-06-08', 1000, 3, 40, 0, '', 'tunda', 0),
(49, '2022-06-08', 1000, 3, 40, 0, '', 'tunda', 0),
(50, '2022-06-08', 1000, 3, 40, 0, '', 'tunda', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `petugas`
--

CREATE TABLE `petugas` (
  `id_petugas` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(128) NOT NULL,
  `fullname` varchar(100) NOT NULL,
  `nama_petugas` varchar(100) NOT NULL,
  `color` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `petugas`
--

INSERT INTO `petugas` (`id_petugas`, `username`, `password`, `fullname`, `nama_petugas`, `color`) VALUES
(4, 'petugas', '33323173616775746570', 'Petugas Naufal V2', 'Petugas Naufal V2', 'dark'),
(5, 'petugasv2', '33323173616775746570', 'Petugas V2', 'Petugas V2', 'dark');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_access_menu`
--

CREATE TABLE `user_access_menu` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user_access_menu`
--

INSERT INTO `user_access_menu` (`id`, `role_id`, `menu_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_sub_menu`
--

CREATE TABLE `user_sub_menu` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `tittle` varchar(128) NOT NULL,
  `url` varchar(128) NOT NULL,
  `icon` varchar(128) NOT NULL,
  `is_active` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user_sub_menu`
--

INSERT INTO `user_sub_menu` (`id`, `menu_id`, `tittle`, `url`, `icon`, `is_active`) VALUES
(1, 1, 'Dashboard', 'petugas', 'fas fa-fw fa-tachometer-alt', 1),
(2, 2, 'My Profile', 'Admin', 'fas fa-fw fa-user', 1);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `view_peminjaman`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `view_peminjaman` (
`nama_anggota` varchar(100)
,`id_anggota` int(11)
,`nama_petugas` varchar(100)
,`judul_buku` varchar(50)
,`id_peminjaman` int(11)
,`tanggal_pinjam` date
,`tanggal_kembali` date
,`status` varchar(30)
);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `view_peminjaman2`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `view_peminjaman2` (
`nama_anggota` varchar(100)
,`id_anggota` int(11)
,`judul_buku` varchar(50)
,`id_buku` int(11)
,`stok_buku` int(11)
,`id_peminjaman` int(11)
,`tanggal_pinjam` date
,`tanggal_kembali` date
,`nama_petugas` varchar(30)
,`status` varchar(30)
,`sesi_id` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `view_pengembalian`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `view_pengembalian` (
`nama_anggota` varchar(100)
,`nama_petugas` varchar(100)
,`judul_buku` varchar(50)
,`denda` int(11)
,`id_pengembalian` int(11)
,`tanggal_pengembalian` date
);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `view_pengembalian2`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `view_pengembalian2` (
`nama_anggota` varchar(100)
,`id_anggota` int(11)
,`judul_buku` varchar(50)
,`denda` int(11)
,`id_pengembalian` int(11)
,`tanggal_pengembalian` date
,`nama_petugas` varchar(30)
,`status` varchar(30)
);

-- --------------------------------------------------------

--
-- Struktur untuk view `view_peminjaman`
--
DROP TABLE IF EXISTS `view_peminjaman`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_peminjaman`  AS SELECT `anggota`.`nama_anggota` AS `nama_anggota`, `anggota`.`id_anggota` AS `id_anggota`, `petugas`.`nama_petugas` AS `nama_petugas`, `buku`.`judul_buku` AS `judul_buku`, `peminjaman`.`id_peminjaman` AS `id_peminjaman`, `peminjaman`.`tanggal_pinjam` AS `tanggal_pinjam`, `peminjaman`.`tanggal_kembali` AS `tanggal_kembali`, `peminjaman`.`status` AS `status` FROM (((`peminjaman` join `anggota` on(`peminjaman`.`id_anggota` = `anggota`.`id_anggota`)) join `buku` on(`peminjaman`.`id_buku` = `buku`.`id_buku`)) join `petugas` on(`peminjaman`.`id_petugas` = `petugas`.`id_petugas`)) ;

-- --------------------------------------------------------

--
-- Struktur untuk view `view_peminjaman2`
--
DROP TABLE IF EXISTS `view_peminjaman2`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_peminjaman2`  AS SELECT `anggota`.`nama_anggota` AS `nama_anggota`, `anggota`.`id_anggota` AS `id_anggota`, `buku`.`judul_buku` AS `judul_buku`, `buku`.`id_buku` AS `id_buku`, `buku`.`stok_buku` AS `stok_buku`, `peminjaman`.`id_peminjaman` AS `id_peminjaman`, `peminjaman`.`tanggal_pinjam` AS `tanggal_pinjam`, `peminjaman`.`tanggal_kembali` AS `tanggal_kembali`, `peminjaman`.`nama_petugas` AS `nama_petugas`, `peminjaman`.`status` AS `status`, `peminjaman`.`sesi_id` AS `sesi_id` FROM ((`peminjaman` join `anggota` on(`peminjaman`.`id_anggota` = `anggota`.`id_anggota`)) join `buku` on(`peminjaman`.`id_buku` = `buku`.`id_buku`)) ;

-- --------------------------------------------------------

--
-- Struktur untuk view `view_pengembalian`
--
DROP TABLE IF EXISTS `view_pengembalian`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pengembalian`  AS SELECT `anggota`.`nama_anggota` AS `nama_anggota`, `petugas`.`nama_petugas` AS `nama_petugas`, `buku`.`judul_buku` AS `judul_buku`, `pengembalian`.`denda` AS `denda`, `pengembalian`.`id_pengembalian` AS `id_pengembalian`, `pengembalian`.`tanggal_pengembalian` AS `tanggal_pengembalian` FROM (((`pengembalian` join `anggota` on(`pengembalian`.`id_anggota` = `anggota`.`id_anggota`)) join `buku` on(`pengembalian`.`id_buku` = `buku`.`id_buku`)) join `petugas` on(`pengembalian`.`id_petugas` = `petugas`.`id_petugas`)) ;

-- --------------------------------------------------------

--
-- Struktur untuk view `view_pengembalian2`
--
DROP TABLE IF EXISTS `view_pengembalian2`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pengembalian2`  AS SELECT `anggota`.`nama_anggota` AS `nama_anggota`, `anggota`.`id_anggota` AS `id_anggota`, `buku`.`judul_buku` AS `judul_buku`, `pengembalian`.`denda` AS `denda`, `pengembalian`.`id_pengembalian` AS `id_pengembalian`, `pengembalian`.`tanggal_pengembalian` AS `tanggal_pengembalian`, `pengembalian`.`nama_petugas` AS `nama_petugas`, `pengembalian`.`status` AS `status` FROM ((`pengembalian` join `anggota` on(`pengembalian`.`id_anggota` = `anggota`.`id_anggota`)) join `buku` on(`pengembalian`.`id_buku` = `buku`.`id_buku`)) ;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indeks untuk tabel `anggota`
--
ALTER TABLE `anggota`
  ADD PRIMARY KEY (`id_anggota`);

--
-- Indeks untuk tabel `announce`
--
ALTER TABLE `announce`
  ADD PRIMARY KEY (`announce_id`);

--
-- Indeks untuk tabel `buku`
--
ALTER TABLE `buku`
  ADD PRIMARY KEY (`id_buku`);

--
-- Indeks untuk tabel `livechat`
--
ALTER TABLE `livechat`
  ADD PRIMARY KEY (`chat_id`);

--
-- Indeks untuk tabel `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD PRIMARY KEY (`id_peminjaman`);

--
-- Indeks untuk tabel `pengembalian`
--
ALTER TABLE `pengembalian`
  ADD PRIMARY KEY (`id_pengembalian`);

--
-- Indeks untuk tabel `petugas`
--
ALTER TABLE `petugas`
  ADD PRIMARY KEY (`id_petugas`);

--
-- Indeks untuk tabel `user_access_menu`
--
ALTER TABLE `user_access_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT untuk tabel `anggota`
--
ALTER TABLE `anggota`
  MODIFY `id_anggota` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT untuk tabel `announce`
--
ALTER TABLE `announce`
  MODIFY `announce_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `buku`
--
ALTER TABLE `buku`
  MODIFY `id_buku` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT untuk tabel `livechat`
--
ALTER TABLE `livechat`
  MODIFY `chat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `peminjaman`
--
ALTER TABLE `peminjaman`
  MODIFY `id_peminjaman` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT untuk tabel `pengembalian`
--
ALTER TABLE `pengembalian`
  MODIFY `id_pengembalian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT untuk tabel `petugas`
--
ALTER TABLE `petugas`
  MODIFY `id_petugas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `user_access_menu`
--
ALTER TABLE `user_access_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-white">Dashboard</h1>
    </div>

    <!-- <h1 class="h3 mb-4 text-white text-center"><?= $title; ?></h1>
    <div class="dropdown-divider mb-3 mt-3"></div> -->

    <!-- Content Row -->
    <div class="row">

        <!-- Buku -->
        <?php foreach ($listData as $data) : ?>
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-warning shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                    <?= $data; ?></div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $listData2[$data]; ?></div>
                            </div>
                            <div class="col-auto">
                                <?php switch ($data):
                                    case 'anggota': ?>
                                        <i class="fas fa-users fa-2x text-gray-300"></i>
                                    <?php break;
                                    default: ?>
                                        <i class="fas fa-book fa-2x text-gray-300"></i>
                                <?php endswitch; ?>
                            </div>
                        </div>
                        <div class="dropdown mt-4">
                            <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Menu Pilihan
                            </button>
                            <div class="dropdown-menu animated--fade-in" aria-labelledby="dropdownMenuButton">
                                <div class="col-auto">
                                    <?php switch ($data):
                                        case 'anggota': ?>
                                            <a class="dropdown-item" href="<?= base_url('admin/dataAnggota') ?>">Data Anggota</a>
                                            <a class="dropdown-item" href="<?= base_url('admin/tambahAnggota') ?>">Tambah Anggota</a>
                                        <?php break;
                                        default: ?>
                                            <a class="dropdown-item" href="<?= base_url('admin/dataBuku') ?>">Data Buku</a>
                                            <a class="dropdown-item" href="<?= base_url('admin/tambahBuku') ?>">Tambah Buku</a>
                                    <?php endswitch; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>

    </div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
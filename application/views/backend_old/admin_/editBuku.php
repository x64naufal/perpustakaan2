                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-white text-center"><?= $title; ?></h1>
                    <div class="dropdown-divider mb-3 mt-3"></div>


                    <div class="row">
                        <div class="col-md-5 container-fluid">

                            <?= form_error('menu', '<div class="alert alert-danger" role="alert">', '</div>');  ?>
                            <?php if ($this->session->flashdata('flash')) : ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    Buku <strong>Berhasil</strong> <?= $this->session->flashdata('flash'); ?>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
					<form action="" method="post">
						<input type="hidden" name="id_buku" value="<?= $listBuku['id_buku']; ?>">
	                    <div class="form-group row">
                            <label for="staticJudul" class="col-sm-2 col-form-label text-white">Judul</label>
                            <div class="col-sm-4">
                                <input type="text" name="judul" class="form-control" id="judul" value="<?= $listBuku['judul_buku']; ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticGenre" class="col-sm-2 col-form-label text-white">Genre</label>
                            <div class="col-sm-2">
                                <select name="genre" id="genre" class="form-control">
                                    <option value="Seni">Seni</option>
                                    <option value="Aksi">Aksi</option>
                                    <option value="Romantis">Romantis</option>
                                    <option value="Komedi">Komedi</option>
                                    <option value="Pelajaran">Pelajaran</option>
                                    <option value="Lainnya">Lainnya</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticPenulis" class="col-sm-2 col-form-label text-white">Penulis</label>
                            <div class="col-sm-3">
                                <input type="text" name="penulis" class="form-control" id="penulis" value="<?= $listBuku['penulis_buku']; ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticPenerbit" class="col-sm-2 col-form-label text-white">Penerbit</label>
                            <div class="col-sm-3">
                                <input type="text" name="penerbit" class="form-control" id="penerbit" value="<?= $listBuku['penerbit_buku']; ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticTahun" class="col-sm-2 col-form-label text-white">Tahun Terbit</label>
                            <div class="col-sm-3">
                                <input type="text" name="tahun" class="form-control" id="tahun" value="<?= $listBuku['tahun_penerbit']; ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticStok" class="col-sm-2 col-form-label text-white">Stok Buku</label>
                            <div class="col-sm-3">
                                <input type="text" name="stok" class="form-control" id="stok" value="<?= $listBuku['stok_buku']; ?>">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-5">
                                <button type="submit" class="btn btn-primary float-right mt-3">Edit</button>   
                            </div>
                        </div>
					</form>
                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->
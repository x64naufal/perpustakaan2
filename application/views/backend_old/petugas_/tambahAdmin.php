                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-white text-center"><?= $title; ?></h1>
                    <div class="dropdown-divider mb-3 mt-3"></div>

                    <div class="row">
                        <div class="col-md-5 container-fluid">

                            <?= form_error('menu', '<div class="alert alert-danger" role="alert">', '</div>');  ?>
                            <?php if ($this->session->flashdata('flash')) : ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    Admin <strong>Berhasil</strong> <?= $this->session->flashdata('flash'); ?>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <form action="" method="post">
                        <!-- <input type="hidden" name="id_anggota"> -->
                        <div class="form-group row">
                            <label for="staticUsername" class="col-sm-2 col-form-label text-white">Username</label>
                            <div class="col-sm-4">
                                <input type="text" name="username" class="form-control" id="username">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticPassword" class="col-sm-2 col-form-label text-white">Password</label>
                            <div class="col-sm-4">
                                <input type="password" name="password" class="form-control" id="password">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticNama" class="col-sm-2 col-form-label text-white">Nama Lengkap</label>
                            <div class="col-sm-4">
                                <input type="text" name="nama" class="form-control" id="nama">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-primary float-right mt-1">Tambah</button>
                            </div>
                        </div>
                        <pre> </pre>
                    </form>
                </div>
                <!-- /.container-fluid -->

                </div>
                <!-- End of Main Content -->
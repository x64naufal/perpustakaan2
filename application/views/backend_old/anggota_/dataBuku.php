                <!-- Begin Page Content -->
                <div class="container-fluid">

                	<!-- Page Heading -->
                	<h1 class="h3 mb-4 text-white text-center"><?= $title; ?></h1>
                	<div class="dropdown-divider mb-3 mt-3"></div>

                	<!-- Search element -->
                	<div class="row">
                		<div class="col-md-5 container-fluid">
                			<form action="<?= base_url('anggota/dataBuku') ?>" method="post">
                				<div class="input-group mb-3">
                					<input type="text" class="form-control" placeholder="Masukkan keyword" name="keyword_buku" autocomplete="off" autofocus>
                					<div class="input-group-append">
                						<input class="btn btn-primary" type="submit" name="submit">
                					</div>
                				</div>
                			</form>
                		</div>
                	</div>

                	<!-- List Anggota -->
                	<div class="row">
                		<div class="col-lg">
                			<!-- <h1 class="h5 mb-4 text-white">Data Buku</h1> -->
                			<!-- <h1 class="h5 mb-4 text-white">Data Buku</h1> -->
                			<form action="" method="post">
                				<h1 class="h5 mb-2 text-white">Results : <?= $total_rows_buku; ?></h1>
                				<table class="table table-bordered" style="background-color: white; color: black;">
                					<!-- <table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%"> -->
                					<thead>
                						<tr>
                							<th scope="col">No</th>
                							<th scope="col">Judul</th>
                							<th scope="col">Genre</th>
                							<th scope="col">Penulis</th>
                							<th scope="col">Penerbit</th>
                							<th scope="col">Tahun Terbit</th>
                							<th scope="col">Stok Buku</th>
                						</tr>
                					</thead>
                					<tbody>
                						<?php if (empty($dataBuku)) : ?>
                							<tr>
                								<td colspan="8">
                									<div class="alert alert-danger" role="alert">
                										Data tidak ditemukan!
                									</div>
                								</td>
                							</tr>
                						<?php endif; ?>
                						<?php foreach ($dataBuku as $bA) : ?>
                							<tr>
                								<th scope="row"><?= ++$startbuku; ?></th>
                								<td><?= $bA['judul_buku']; ?></td>
                								<td><?= $bA['genre_buku']; ?></td>
                								<td><?= $bA['penulis_buku']; ?></td>
                								<td><?= $bA['penerbit_buku']; ?></td>
                								<td><?= $bA['tahun_penerbit']; ?></td>
                								<td><?= $bA['stok_buku']; ?></td>
                							</tr>
                						<?php endforeach; ?>
                					</tbody>
                				</table>
                				<?= $this->pagination->create_links(); ?>
                		</div>
                	</div>

                </div>
                <!-- /.container-fluid -->

                </div>
                <!-- End of Main Content -->
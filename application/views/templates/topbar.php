        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content" style="<?= $userColor; ?>">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>



                    <!-- Topbar Navbar -->
                    <ul class="navbar-nav ml-auto">

                        <!-- Nav Item - Alerts -->
                        <li class="nav-item dropdown no-arrow mx-1">
                            <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-bell fa-fw"></i>
                                <span class="badge badge-danger badge-counter"><?= $amountMsg; ?></span>
                            </a>
                            <!-- Dropdown - Alerts -->
                            <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown" style="height: 380px; overflow: auto;">
                                <h6 class="dropdown-header">
                                    Announce!
                                </h6>
                                <?php foreach ($newMsg as $datax) : ?>
                                    <a class="dropdown-item d-flex align-items-center" href="#">
                                        <div class="mr-3">
                                            <div class="icon-circle bg-primary">
                                                <i class="fas fa-user text-white"></i>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="small text-gray-500"><?= $datax['announce_user']; ?> | <?= $datax['announce_date']; ?></div>
                                            <?= str_replace('\r\n', '<br>', $datax['announce_msg']); ?>
                                        </div>
                                    </a>
                                <?php endforeach; ?>
                                <a class="dropdown-item text-center small text-gray-500" href="#">Show All Announce</a>
                            </div>
                        </li>

                        <?php if ($sessionUser == 'PETUGAS_USER') : ?>
                            <span class="mr-2 mt-4 d-none d-lg-inline text-gray-600 small">Petugas</span>
                        <?php elseif ($sessionUser == 'ADMIN_USER') : ?>
                            <span class="mr-2 mt-4 d-none d-lg-inline text-gray-600 small">Admin</span>
                        <?php elseif ($sessionUser == 'ANGGOTA_USER') : ?>
                            <span class="mr-2 mt-4 d-none d-lg-inline text-gray-600 small">Anggota</span>
                        <?php endif; ?>
                        <div class="topbar-divider d-none d-sm-block"></div>

                        <!-- Nav Item - User Information -->
                        <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <?php if (!$user['fullname']) : ?>
                                    <?= redirect('auth/blocked'); ?>
                                <?php else : ?>
                                    <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?= $user['fullname']; ?></span>
                                    <span class="fas fa-user"> </span>
                                <?php endif; ?>
                            </a>
                            <!-- Dropdown - User Information -->
                            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                                <!-- <a class="dropdown-item" href="#">
                                    <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                    My Profile
                                </a> -->
                                <!-- <div class="dropdown-divider"></div> -->
                                <div class="dropdown-item"></div>
                                <a class="dropdown-item" href="<?= base_url('auth/logout'); ?>" data-toggle="modal" data-target="#logoutModal">
                                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Logout
                                </a>
                            </div>
                        </li>

                    </ul>

                </nav>
                <!-- End of Topbar -->
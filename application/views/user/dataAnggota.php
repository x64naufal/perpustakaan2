                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-white text-center"><?= $title; ?></h1>
                    <div class="dropdown-divider mb-3 mt-3"></div>

                    <!-- Search element -->
                    <div class="row">
                        <div class="col-md-5 container-fluid">
                            <?php if ($sessionUser == 'PETUGAS_USER' || $sessionUser == 'ADMIN_USER') : ?>
                                <form action="<?= base_url($setDataAnggota[$sessionUser]); ?>/dataAnggota/" method="post">
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" placeholder="Masukkan keyword" name="keyword_anggota" autocomplete="off" autofocus>
                                        <div class="input-group-append">
                                            <input class="btn btn-primary" type="submit" name="submit">
                                        </div>
                                    </div>
                                </form>
                                <?= form_error('menu', '<div class="alert alert-danger" role="alert">', '</div>');  ?>
                                <?php if ($this->session->flashdata('flash')) : ?>
                                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                                        Anggota <strong>Berhasil</strong> <?= $this->session->flashdata('flash'); ?>
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>
                    </div>

                    <!-- List Anggota -->
                    <div class="row">
                        <div class="col-lg" style="overflow: auto;">
                            <form action="" method="post">
                                <div class="row">
                                    <div class="col-sm-4 mb-2">
                                        <button type="submit" class="btn btn-primary mb-2"><a href="<?= base_url($setDataAnggota[$sessionUser]); ?>/" class="text-white text-decoration-none">Kembali</a></button>
                                    </div>
                                </div>
                                <h1 class="h5 mb-2 text-white">Results : <?= $total_rows_anggota; ?></h1>
                                <table class="table table-bordered" style="background-color: white; color: black;">
                                    <thead>
                                        <tr>
                                            <th scope="col">No</th>
                                            <th scope="col">Nama</th>
                                            <th scope="col">Username</th>
                                            <th scope="col">Jenis Kelamin</th>
                                            <th scope="col">Kelas</th>
                                            <th scope="col">Jurusan</th>
                                            <th scope="col">No Telpon</th>
                                            <th scope="col">Alamat</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (empty($dataAnggota)) : ?>
                                            <tr>
                                                <td colspan="8">
                                                    <div class="alert alert-danger" role="alert">
                                                        Data tidak ditemukan!
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endif; ?>
                                        <?php foreach ($dataAnggota as $uA) : ?>
                                            <tr>
                                                <th scope="row"><?= ++$startanggota; ?></th>
                                                <td><?= $uA['fullname']; ?></td>
                                                <td><?= $uA['username']; ?></td>
                                                <td><?= $uA['jk_anggota']; ?></td>
                                                <td><?= $uA['kelas_anggota']; ?></td>
                                                <td><?= $uA['jurusan_anggota']; ?></td>
                                                <td><?= $uA['no_telp_anggota']; ?></td>
                                                <td><?= $uA['alamat_anggota']; ?></td>
                                                <?php if ($sessionUser == 'PETUGAS_USER' || $sessionUser == 'ADMIN_USER') : ?>
                                                    <td>
                                                        <a href="<?= base_url($setDataAnggota[$sessionUser]); ?>/editAnggota/<?= $uA['id_anggota']; ?>" class="badge badge-success"><span class="fas fa-fw fa-edit"></span> edit</a>
                                                        <a href="<?= base_url($setDataAnggota[$sessionUser]); ?>/hapusAnggota/<?= $uA['id_anggota']; ?>" class="badge badge-danger" onclick="return confirm('Are you sure you want to delete it?')"><span class="fas fa-fw fa-trash-alt"></span> delete</a>
                                                    </td>
                                                <?php endif; ?>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <?= $this->pagination->create_links(); ?>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

                </div>
                <!-- End of Main Content -->
                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-white text-center"><?= $title; ?></h1>
                    <div class="dropdown-divider mb-3 mt-3"></div>


                    <div class="row">
                        <div class="col-md-5 container-fluid">

                            <?= form_error('menu', '<div class="alert alert-danger" role="alert">', '</div>');  ?>
                            <?php if ($this->session->flashdata('flash')) : ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    Anggota <strong>Berhasil</strong> <?= $this->session->flashdata('flash'); ?>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <form action="" method="post">
                        <input type="hidden" name="id_anggota" value="<?= $userAnggota['id_anggota']; ?>">
                        <div class="form-group row">
                            <label for="staticUsername" class="col-sm-2 col-form-label text-white">Username</label>
                            <div class="col-sm-4">
                                <input type="text" name="username" class="form-control" id="username" value="<?= $userAnggota['username']; ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticNama" class="col-sm-2 col-form-label text-white">Nama</label>
                            <div class="col-sm-4">
                                <input type="text" name="nama" class="form-control" id="nama" value="<?= $userAnggota['fullname']; ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticJK" class="col-sm-2 col-form-label text-white">Jenis Kelamin</label>
                            <div class="col-sm-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="jk" id="jk1" value="L">
                                    <label class="form-check-label text-white" for="laki">
                                        Laki-Laki
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="jk" id="jk2" value="P">
                                    <label class="form-check-label text-white" for="perempuan">
                                        Perempuan
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticKelas" class="col-sm-2 col-form-label text-white">Kelas</label>
                            <div class="col-sm-2">
                                <select name="kelas" id="kelas" class="form-control">
                                    <option value="XI RPL A">XI RPL A</option>
                                    <option value="XI RPL B">XI RPL B</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticJurusan" class="col-sm-2 col-form-label text-white">Jurusan</label>
                            <div class="col-sm-3">
                                <select name="jurusan" id="jurusan" class="form-control">
                                    <option value="Rekayasa Perangkat Lunak">Rekayasa Perangkat Lunak</option>
                                    <option value="Teknik Komputer dan Jaringan">Teknik Komputer dan Jaringan</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticNoTelp" class="col-sm-2 col-form-label text-white">Nomor Telepon</label>
                            <div class="col-sm-3">
                                <input type="text" name="no_telp" class="form-control" id="no_telp" value="<?= $userAnggota['no_telp_anggota']; ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticAlamat" class="col-sm-2 col-form-label text-white">Alamat</label>
                            <div class="col-sm-6">
                                <input type="text" name="alamat" class="form-control" id="alamat" value="<?= $userAnggota['alamat_anggota']; ?>">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-8">
                                <button type="submit" class="btn btn-primary float-right mt-3">Edit</button>
                                <button type="submit" class="btn btn-primary float-right mt-3 mr-4"><a href="<?= base_url($setDataCom[$sessionUser]); ?>/dataAnggota/" class="text-white text-decoration-none">Kembali</a></button>

                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.container-fluid -->

                </div>
                <!-- End of Main Content -->
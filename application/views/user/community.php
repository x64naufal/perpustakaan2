                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <div class="row">

                        <!-- Area Chart -->
                        <div class="col-xl-8 col-lg-7">
                            <div class="card shadow mb-4" style="overflow: auto; height: 530px;">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">Livechat Community</h6>
                                </div>
                                <style>
                                    .container {
                                        border: 2px solid #dedede;
                                        background-color: #f1f1f1;
                                        border-radius: 5px;
                                        padding: 10px;
                                        margin: 10px 0;
                                    }

                                    .darker {
                                        border-color: #ccc;
                                        background-color: #ddd;
                                    }

                                    .container::after {
                                        content: "";
                                        clear: both;
                                        display: table;
                                    }

                                    .container img {
                                        float: left;
                                        max-width: 60px;
                                        width: 100%;
                                        margin-right: 20px;
                                        border-radius: 50%;
                                    }

                                    .container img.right {
                                        float: right;
                                        margin-left: 20px;
                                        margin-right: 0;
                                    }

                                    .time-right {
                                        float: right;
                                        color: #aaa;
                                    }

                                    .time-left {
                                        float: left;
                                        color: #999;
                                    }
                                </style>
                                <?php foreach ($newChat as $datax) : ?>
                                    <div class="container">
                                        <!-- //<img src="https://lh3.googleusercontent.com/proxy/8XJVw8RDH7V-NsKtYXqOkeBcXSSC8CUvSSdwXlr84-RPPMHNzFw8SG-dyWCR4a806AUEKGWmuNQJYwVyXV-9aAFIpbAUz28" alt="Avatar" style="width:100%;"> -->
                                        <div class="time-left">
                                            <div class="small text-gray-500"><?= $datax['chat_session']; ?> | <?= $datax['chat_date']; ?></div>
                                            <?= str_replace('\r\n', '<br>', $datax['chat_msg']); ?>
                                        </div>
                                        <i class="time-right icon-circle bg-primary fas fa-user text-white"></i>
                                        <!-- <span class="time-right"><?= $datax['chat_user']; ?></span> -->
                                    </div>
                                <?php endforeach; ?>

                            </div>
                        </div>

                        <!-- Pie Chart -->
                        <div class="col-xl-4 col-lg-5">
                            <div class="card shadow mb-4">
                                <!-- Card Header - Dropdown -->
                                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">History</h6>
                                </div>
                                <div class="card-body">
                                    <p>Livechat is a public page that serves to conduct questions and answers between individuals or communities.</p>
                                    <p class="mb-0">This page was created with the developer in mind to facilitate interaction between users and library admins.</p>
                                </div>
                                <button class="open-button" onclick="openForm()">Chat</button>

                                <div class="chat-popup" id="myForm">
                                    <form action="" class="form-container" method="post">
                                        <h1>Livechat</h1>

                                        <label for="msg"><b>Message</b></label>
                                        <textarea placeholder="Messages" name="msg" id="msg" required></textarea>

                                        <button type="submit" class="btn">Send</button>
                                        <button type="button" class="btn cancel" onclick="closeForm()">Close</button>
                                    </form>
                                </div>

                                <script>
                                    function openForm() {
                                        document.getElementById("myForm").style.display = "block";
                                    }

                                    function closeForm() {
                                        document.getElementById("myForm").style.display = "none";
                                    }
                                </script>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4 mb-2">
                            <button type="submit" class="btn btn-primary mb-2"><a href="<?= base_url($setDataCom[$sessionUser]); ?>/" class="text-white text-decoration-none">Kembali</a></button>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

                </div>
                <!-- End of Main Content -->
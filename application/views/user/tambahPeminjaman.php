                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-white text-center"><?= $title; ?></h1>
                    <div class="dropdown-divider mb-3 mt-3"></div>


                    <div class="row">
                        <div class="col-md-5 container-fluid">
                            <?php if ($sessionUser) : ?>
                                <?= form_error('menu', '<div class="alert alert-danger" role="alert">', '</div>');  ?>
                                <?php if ($this->session->flashdata('flash')) : ?>
                                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                                        Transaksi <strong>Berhasil</strong> <?= $this->session->flashdata('flash'); ?>
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <form action="" method="post">

                        <div class="form-group row">
                            <label for="staticPetugas" class="col-sm-2 col-form-label text-white">Anggota</label>
                            <div class="col-sm-2">
                                <select name="anggota" id="anggota" class="form-control">
                                    <?php if ($sessionUser == 'PETUGAS_USER' || $sessionUser == 'ADMIN_USER') : ?>
                                        <?php foreach ($listuser as $lu) : ?>
                                            <option value="<?= $lu['id_anggota']; ?>"><?= $lu['nama_anggota']; ?> (<?= $lu['username']; ?>)</option>
                                        <?php endforeach; ?>
                                    <?php else : ?>
                                        <option value="<?= $user['id_anggota']; ?>"><?= $user['nama_anggota']; ?> (<?= $user['username']; ?>)</option>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="staticJudul" class="col-sm-2 col-form-label text-white">Judul buku</label>
                            <div class="col-sm-2">
                                <select name="judul" id="judul" class="form-control">
                                    <?php foreach ($listbuku as $lb) : ?>
                                        <option value="<?= $lb['id_buku']; ?>"><?= $lb['judul_buku']; ?> (<?= $lb['genre_buku']; ?>)</option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="staticWK" class="col-sm-2 col-form-label text-white">Waktu peminjaman</label>
                            <div class="col-sm-2">
                                <input type="date" class="form-control" name="wk1" value="<?= date("Y-m-d", strtotime('now')); ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticWK" class="col-sm-2 col-form-label text-white">Waktu pengembalian</label>
                            <div class="col-sm-2">
                                <input type="date" class="form-control" name="wk2">
                            </div>
                        </div>

                        <!-- <div class="form-group row">
                            <label for="staticStok" class="col-sm-2 col-form-label text-white">Stok Buku</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" name="stok" value="" readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="staticSesi" class="col-sm-2 col-form-label text-white">Sesi ID</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" name="sesi" value="" readonly>
                            </div>
                        </div> -->

                        <div class="row">
                            <div class="col-sm-4">
                                <button type="submit" class="btn btn-primary float-right mt-1">Tambahkan</button>
                                <button type="submit" class="btn btn-primary float-right mt-1 mr-4"><a href="<?= base_url($setDataCom[$sessionUser]); ?>/peminjaman/" class="text-white text-decoration-none">Kembali</a></button>
                            </div>
                        </div>
                        <pre> </pre>
                    </form>

                </div>
                <!-- /.container-fluid -->

                </div>
                <!-- End of Main Content -->
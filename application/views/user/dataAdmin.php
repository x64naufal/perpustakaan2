                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-white text-center"><?= $title; ?></h1>
                    <div class="dropdown-divider mb-3 mt-3"></div>
                    <?php if ($sessionUser) : ?>
                        <!-- Search element -->
                        <div class="row">
                            <div class="col-md-5 container-fluid">
                                <form action="<?= base_url($setDataAdmin[$sessionUser]); ?>/dataAdmin/" method="post">
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" placeholder="Masukkan keyword" name="keyword_admin" autocomplete="off" autofocus>
                                        <div class="input-group-append">
                                            <input class="btn btn-primary" type="submit" name="submit">
                                        </div>
                                    </div>
                                </form>
                                <?= form_error('menu', '<div class="alert alert-danger" role="alert">', '</div>');  ?>
                                <?php if ($this->session->flashdata('flash')) : ?>
                                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                                        Admin <strong>Berhasil</strong> <?= $this->session->flashdata('flash'); ?>
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>

                        <!-- List Admin -->
                        <div class="row">
                            <div class="col-lg" style="overflow: auto;">
                                <!-- <h1 class="h5 mb-4 text-white">Data Admin</h1> -->
                                <form action="" method="post">
                                    <div class="row">
                                        <div class="col-sm-4 mb-2">
                                            <button type="submit" class="btn btn-primary mb-2"><a href="<?= base_url($setDataAdmin[$sessionUser]); ?>/" class="text-white text-decoration-none">Kembali</a></button>
                                        </div>
                                    </div>
                                    <h1 class="h5 mb-2 text-white">Results : <?= $total_rows_admin; ?></h1>
                                    <table class="table table-bordered" style="background-color: white; color: black;">
                                        <!-- <table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%"> -->
                                        <thead>
                                            <tr>
                                                <th scope="col">No</th>
                                                <th scope="col">Nama</th>
                                                <th scope="col">Username</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if (empty($dataAdmin)) : ?>
                                                <tr>
                                                    <td colspan="8">
                                                        <div class="alert alert-danger" role="alert">
                                                            Data tidak ditemukan!
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php endif; ?>
                                            <?php foreach ($dataAdmin as $dA) : ?>
                                                <tr>
                                                    <th scope="row"><?= ++$startadmin; ?></th>
                                                    <td><?= $dA['fullname']; ?></td>
                                                    <td><?= $dA['username']; ?></td>
                                                    <td>
                                                        <a href="<?= base_url($setDataAdmin[$sessionUser]); ?>/editAdmin/<?= $dA['id_admin']; ?>" class="badge badge-success"><span class="fas fa-fw fa-edit"></span> edit</a>
                                                        <a href="<?= base_url($setDataAdmin[$sessionUser]); ?>/hapusAdmin/<?= $dA['id_admin']; ?>" class="badge badge-danger" onclick="return confirm('Are you sure you want to delete it?')"><span class="fas fa-fw fa-trash-alt"></span> delete</a>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                            </div>
                        </div>
                    <?php endif; ?>

                </div>
                <!-- /.container-fluid -->

                </div>
                <!-- End of Main Content -->
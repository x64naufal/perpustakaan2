                <!-- Begin Page Content -->
                <div class="container-fluid">


                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-white text-center"><?= $title; ?></h1>
                    <div class="dropdown-divider mb-3 mt-3"></div>

                    <div class="row">
                        <div class="col-md-5 container-fluid">
                            <?= form_error('menu', '<div class="alert alert-danger" role="alert">', '</div>');  ?>
                            <?php if ($this->session->flashdata('flash')) : ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    Peminjaman <strong>Berhasil</strong> <?= $this->session->flashdata('flash'); ?>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <form action="" method="post">
                        <input type="hidden" name="id_peminjaman" value="<?= $peminjamans['id_peminjaman']; ?>">
                        <div class="form-group row">
                            <label for="staticNama" class="col-sm-2 col-form-label text-white">Anggota</label>
                            <div class="col-sm-2">
                                <input type="text" name="anggota" class="form-control" id="anggota" value="<?= $peminjamans['nama_anggota']; ?>" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticJudul" class="col-sm-2 col-form-label text-white">Judul buku</label>
                            <div class="col-sm-2">
                                <select name="judul" id="judul" class="form-control">
                                    <?php
                                    // $xyz = $peminjamans['id_buku'];
                                    $no = 0;
                                    $arr = array();
                                    ?>
                                    <?php foreach ($listbuku as $lb) : ?>
                                        <?php if ($peminjamans['judul_buku'] == $lb['judul_buku']) : ?>
                                            <?php
                                            $arr = [
                                                'id_buku' => $lb['id_buku'],
                                                'judul_buku' => $peminjamans['judul_buku'],
                                                'genre_buku' => $lb['genre_buku']
                                            ];
                                            ?>
                                        <?php endif; ?>
                                    <?php endforeach; ?>

                                    <?php foreach ($listbuku as $lc) : ?>
                                        <?php if ($no == 0) : ?>
                                            <option value="<?= $arr['id_buku']; ?>" class="bg-dark text-white"><?= $arr['judul_buku']; ?> (<?= $arr['genre_buku']; ?>)</option>
                                        <?php else : ?>
                                            <option value="<?= $lc['id_buku']; ?>"><?= $lc['judul_buku']; ?> (<?= $lc['genre_buku']; ?>)</option>
                                        <?php endif; ?>
                                        <?= $no++; ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="staticWK" class="col-sm-2 col-form-label text-white">Waktu peminjaman</label>
                            <div class="col-sm-2">
                                <input type="date" class="form-control" name="wk1" value="<?= $peminjamans['tanggal_pinjam']; ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticWK" class="col-sm-2 col-form-label text-white">Waktu pengembalian</label>
                            <div class="col-sm-2">
                                <input type="date" class="form-control" name="wk2" value="<?= $peminjamans['tanggal_kembali']; ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticStatus" class="col-sm-2 col-form-label text-white">Status</label>
                            <div class="col-sm-2">
                                <select name="status" id="status" class="form-control">
                                    <option value="terima">Terima</option>
                                    <option value="tunggu">Tunda</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <button type="submit" class="btn btn-primary float-right mt-1">Edit</button>
                                <button type="submit" class="btn btn-primary float-right mt-1 mr-4"><a href="<?= base_url($setDataCom[$sessionUser]); ?>/peminjaman/" class="text-white text-decoration-none">Kembali</a></button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.container-fluid -->

                </div>
                <!-- End of Main Content -->
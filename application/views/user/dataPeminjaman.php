<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-white text-center"><?= $title; ?></h1>
    <div class="dropdown-divider mb-3 mt-3"></div>

    <!-- Search element -->
    <div class="row">
        <div class="col-md-5 container-fluid">

            <?php if ($sessionUser) : ?>
                <form action="<?= base_url($setDataPeminjam[$sessionUser]); ?>/peminjaman/" method="post">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Masukkan keyword" name="keyword_user" autocomplete="off" autofocus>
                        <div class="input-group-append">
                            <input class="btn btn-primary" type="submit" name="submit">
                        </div>
                    </div>
                </form>
                <?php if ($sessionUser == 'PETUGAS_USER' || $sessionUser == 'ADMIN_USER') : ?>
                    <?= form_error('menu', '<div class="alert alert-danger" role="alert">', '</div>');  ?>
                    <?php if ($this->session->flashdata('flash')) : ?>
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            Peminjaman <strong>Berhasil</strong> <?= $this->session->flashdata('flash'); ?>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>
            <?php endif; ?>
        </div>
    </div>

    <!-- List Anggota -->
    <div class="row">
        <div class="col-lg" style="overflow: auto;">
            <form action="" method="post">
                <!-- <h1 class="h5 mb-2 text-white">Results : <?= $total_rows_peminjam; ?></h1> -->
                <div class="row">
                    <div class="col-sm-4 mb-2">
                        <button type="submit" class="btn btn-primary mb-2"><a href="<?= base_url($setDataPeminjam[$sessionUser]); ?>/" class="text-white text-decoration-none">Kembali</a></button>
                    </div>
                </div>
                <h1 class="h5 mb-2 text-white">Results : <?= $total_rows_peminjam; ?></h1>
                <table class="table table-bordered" style="background-color: white; color: black;">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Nama Peminjam</th>
                            <th scope="col">Judul Buku</th>
                            <th scope="col">Admin Penerima</th>
                            <th scope="col">Tanggal Pinjam</th>
                            <th scope="col">Tanggal Kembali</th>
                            <th scope="col">Status</th>
                            <th scope="col">Info</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (empty($dataPeminjam)) : ?>
                            <tr>
                                <td colspan="8">
                                    <div class="alert alert-danger" role="alert">
                                        Data tidak ditemukan!
                                    </div>
                                </td>
                            </tr>
                        <?php endif; ?>
                        <?php foreach ($dataPeminjam as $bp) : ?>
                            <tr>
                                <th scope="row"><?= ++$startpeminjam; ?></th>
                                <td><?= $bp['nama_anggota']; ?></td>
                                <td><?= $bp['judul_buku']; ?></td>
                                <td><?= $bp['nama_petugas']; ?></td>
                                <td><?= $bp['tanggal_pinjam']; ?></td>
                                <td><?= $bp['tanggal_kembali']; ?></td>

                                <?php
                                $time = date('Y-m-d', strtotime('now'));
                                $kk = date('d', strtotime($bp['tanggal_kembali']) - strtotime($bp['tanggal_pinjam']));
                                // $diff = date_diff(date_create($bp['tanggal_pinjam']), date_create($bp['tanggal_kembali']));
                                $diff = date_diff(date_create($bp['tanggal_kembali']), date_create($time));
                                $dendas = 1 * $diff->format('%a') * 1000;
                                // print_r($kk);
                                ?>
                                <?php if (date("Y-m-d") >= date($bp['tanggal_kembali'])) : ?>
                                    <td>
                                        <span class="text-center btn btn-danger">Denda (Rp. <?= $dendas; ?>)</span>
                                    </td>
                                <?php else : ?>
                                    <?php if ($bp['status'] == 'terima') : ?>
                                        <td>
                                            <span class="text-center btn btn-success">Disetujui</span>
                                        </td>
                                    <?php else : ?>
                                        <td>
                                            <span class="text-center btn btn-warning">Menunggu Konfirmasi</span>
                                        </td>
                                    <?php endif; ?>
                                <?php endif; ?>
                                <td>
                                    <?php if ($sessionUser == 'ANGGOTA_USER' && $user['fullname'] == $bp['nama_anggota']) : ?>
                                        <a class="anjai nav-link text-center dropdown btn btn-light" id="ddnih" data-toggle="dropdown" aria-expanded="false">
                                            More
                                        </a>
                                        <?php if ($bp['status'] != "tunda") : ?>
                                            <ul class="dropdown-menu" aria-labelledby="ddnih">
                                                <li>
                                                    <a class="anjai2 nav-link btn-success text-white" href="<?= base_url($setDataPeminjam[$sessionUser]); ?>/ajukanPengembalian/<?= $bp['id_peminjaman']; ?>">
                                                        <span class="iconify" data-icon="bytesize:edit"></span>
                                                        Pengembelian
                                                    </a>
                                                </li>
                                            </ul>
                                        <?php endif; ?>
                                    <?php elseif ($sessionUser == 'PETUGAS_USER' || $sessionUser == 'ADMIN_USER') : ?>
                                        <a class="anjai nav-link text-center dropdown btn btn-light" id="ddnih" data-toggle="dropdown" aria-expanded="false">
                                            More
                                        </a>
                                        <ul class="dropdown-menu" aria-labelledby="ddnih">
                                            <li>
                                                <a class="anjai2 nav-link btn-success text-white" href="<?= base_url($setDataPeminjam[$sessionUser]); ?>/ajukanPengembalian/<?= $bp['id_peminjaman']; ?>">
                                                    <span class="iconify" data-icon="bytesize:edit"></span>
                                                    Pengembelian
                                                </a>
                                            </li>
                                            <li>
                                                <a class="anjai2 nav-link btn-primary text-white" href="<?= base_url($setDataPeminjam[$sessionUser]); ?>/editPeminjaman/<?= $bp['id_peminjaman']; ?>">
                                                    <span class="iconify" data-icon="bytesize:edit"></span>
                                                    Edit
                                                </a>
                                            </li>
                                            <li>
                                                <a class="anjai2 nav-link btn-danger text-white" href="<?= base_url($setDataPeminjam[$sessionUser]); ?>/hapusPeminjaman/<?= $bp['id_peminjaman']; ?>" onclick="return confirm('Are you sure you want to delete it?')">
                                                    <span class="iconify" data-icon="fluent:delete-20-regular"></span>
                                                    Delete
                                                </a>
                                            </li>
                                        </ul>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <?= $this->pagination->create_links(); ?>
        </div>
    </div>
    <!-- date_create_immutable_from_format() -->

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
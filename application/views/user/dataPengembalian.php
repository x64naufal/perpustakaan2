<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-white text-center"><?= $title; ?></h1>
    <div class="dropdown-divider mb-3 mt-3"></div>

    <!-- Search element -->
    <div class="row">
        <div class="col-md-5 container-fluid">

            <?php if ($sessionUser) : ?>
                <form action="<?= base_url($setDataPeminjam[$sessionUser]); ?>/pengembalian/" method="post">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Masukkan keyword" name="keyword_user" autocomplete="off" autofocus>
                        <div class="input-group-append">
                            <input class="btn btn-primary" type="submit" name="submit">
                        </div>
                    </div>
                </form>
                <?php if ($sessionUser == 'PETUGAS_USER' || $sessionUser == 'ADMIN_USER') : ?>
                    <?= form_error('menu', '<div class="alert alert-danger" role="alert">', '</div>');  ?>
                    <?php if ($this->session->flashdata('flash')) : ?>
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            Peminjaman <strong>Berhasil</strong> <?= $this->session->flashdata('flash'); ?>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>
            <?php endif; ?>
        </div>
    </div>

    <!-- List Anggota -->
    <div class="row">
        <div class="col-lg" style="overflow: auto;">
            <form action="" method="post">
                <div class="row">
                    <div class="col-sm-4 mb-2">
                        <button type="submit" class="btn btn-primary mb-2"><a href="<?= base_url($setDataPeminjam[$sessionUser]); ?>/" class="text-white text-decoration-none">Kembali</a></button>
                    </div>
                </div>
                <h1 class="h5 mb-2 text-white">Results : <?= $total_rows_pengembalian; ?></h1>
                <table class="table table-bordered" style="background-color: white; color: black;">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Nama Pengembalian</th>
                            <th scope="col">Judul Buku</th>
                            <th scope="col">Admin Penerima</th>
                            <th scope="col">Tanggal Kembali</th>
                            <th scope="col">Denda</th>
                            <th scope="col">Status</th>
                            <?php if ($sessionUser == 'PETUGAS_USER' || $sessionUser == 'ADMIN_USER') : ?>
                                <th scope="col">Info</th>
                            <?php endif; ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (empty($dataPengembalian)) : ?>
                            <tr>
                                <td colspan="8">
                                    <div class="alert alert-danger" role="alert">
                                        Data tidak ditemukan!
                                    </div>
                                </td>
                            </tr>
                        <?php endif; ?>
                        <?php foreach ($dataPengembalian as $bp) : ?>
                            <tr>
                                <th scope="row"><?= ++$startpengembalian; ?></th>
                                <td><?= $bp['nama_anggota']; ?></td>
                                <td><?= $bp['judul_buku']; ?></td>
                                <td><?= $bp['nama_petugas']; ?></td>
                                <td><?= $bp['tanggal_pengembalian']; ?></td>
                                <td><?= $bp['denda']; ?></td>
                                <?php if ($bp['status'] == 'terima') : ?>
                                    <td>
                                        <span class="text-center btn btn-success">Disetujui</span>
                                    </td>
                                <?php else : ?>
                                    <td>
                                        <span class="text-center btn btn-warning">Menunggu Konfirmasi</span>
                                    </td>
                                <?php endif; ?>
                                <?php if ($sessionUser == 'PETUGAS_USER' || $sessionUser == 'ADMIN_USER') : ?>
                                    <td>
                                        <a class="anjai nav-link text-center dropdown btn btn-light" id="ddnih" data-toggle="dropdown" aria-expanded="false">
                                            More
                                        </a>
                                        <ul class="dropdown-menu" aria-labelledby="ddnih">
                                            <li>
                                                <a class="anjai2 nav-link btn-primary text-white" href="<?= base_url($setDataPeminjam[$sessionUser]); ?>/editPengembalian/<?= $bp['id_pengembalian']; ?>">
                                                    <span class="iconify" data-icon="bytesize:edit"></span>
                                                    Edit
                                                </a>
                                            </li>
                                            <li>
                                            <li>
                                                <a class="anjai2 nav-link btn-danger text-white" href="<?= base_url($setDataPeminjam[$sessionUser]); ?>/hapusPengembalian/<?= $bp['id_pengembalian']; ?>" onclick="return confirm('Are you sure you want to delete it?')">
                                                    <span class="iconify" data-icon="fluent:delete-20-regular"></span>
                                                    Delete
                                                </a>
                                            </li>
                                        </ul>
                                    </td>
                                <?php endif; ?>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <?= $this->pagination->create_links(); ?>
        </div>
    </div>
    <!-- date_create_immutable_from_format() -->

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
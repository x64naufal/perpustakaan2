<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Full backend role. Method by Naufalagler -->

    <!-- <canvas id="myChart" style="width:100%;max-width:600px; background-color: #000000cc"></canvas> -->

    <?php switch ($sessionUser):

        case 'PETUGAS_USER': ?>
            <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-white">Dashboard</h1>
            </div>

            <div class="row">
                <!-- Buku -->
                <?php foreach ($listData as $data) : ?>
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border j88-left-warning shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            <?= $data; ?></div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $listData2[$data]; ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <?php switch ($data):
                                            case 'admin': ?>
                                                <div class="col-auto">
                                                    <i class="fas fa-users fa-2x text-gray-300"></i>
                                                </div>
                                            <?php break;
                                            case 'anggota': ?>
                                                <i class="fas fa-users fa-2x text-gray-300"></i>
                                            <?php break;
                                            default: ?>
                                                <i class="fas fa-book fa-2x text-gray-300"></i>
                                        <?php endswitch; ?>
                                    </div>
                                </div>
                                <div class="dropdown mt-4">
                                    <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Menu Pilihan
                                    </button>
                                    <div class="dropdown-menu animated--fade-in" aria-labelledby="dropdownMenuButton">
                                        <?php switch ($data):
                                            case 'admin': ?>
                                                <a class="dropdown-item" href="<?= base_url('petugas/dataAdmin') ?>">Data Admin</a>
                                                <a class="dropdown-item" href="<?= base_url('petugas/tambahAdmin') ?>">Tambah Admin</a>
                                            <?php break;
                                            case 'anggota': ?>
                                                <a class="dropdown-item" href="<?= base_url('petugas/dataAnggota') ?>">Data Anggota</a>
                                                <a class="dropdown-item" href="<?= base_url('petugas/tambahAnggota') ?>">Tambah Anggota</a>
                                            <?php break;
                                            default: ?>
                                                <a class="dropdown-item" href="<?= base_url('petugas/dataBuku') ?>">Data Buku</a>
                                                <a class="dropdown-item" href="<?= base_url('petugas/tambahBuku') ?>">Tambah Buku</a>
                                        <?php endswitch; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php break;
        case 'ADMIN_USER': ?>
            <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-white">Dashboard</h1>
            </div>

            <div class="row">
                <!-- Buku -->
                <?php foreach ($listData as $data) : ?>
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-warning shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            <?= $data; ?></div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $listData2[$data]; ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <?php switch ($data):
                                            case 'anggota': ?>
                                                <i class="fas fa-users fa-2x text-gray-300"></i>
                                            <?php break;
                                            default: ?>
                                                <i class="fas fa-book fa-2x text-gray-300"></i>
                                        <?php endswitch; ?>
                                    </div>
                                </div>
                                <div class="dropdown mt-4">
                                    <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Menu Pilihan
                                    </button>
                                    <div class="dropdown-menu animated--fade-in" aria-labelledby="dropdownMenuButton">
                                        <div class="col-auto">
                                            <?php switch ($data):
                                                case 'anggota': ?>
                                                    <a class="dropdown-item" href="<?= base_url('admin/dataAnggota') ?>">Data Anggota</a>
                                                    <a class="dropdown-item" href="<?= base_url('admin/tambahAnggota') ?>">Tambah Anggota</a>
                                                <?php break;
                                                default: ?>
                                                    <a class="dropdown-item" href="<?= base_url('admin/dataBuku') ?>">Data Buku</a>
                                                    <a class="dropdown-item" href="<?= base_url('admin/tambahBuku') ?>">Tambah Buku</a>
                                            <?php endswitch; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php break;
        case 'ANGGOTA_USER': ?>
            <!-- Page Heading -->
            <h1 class="h3 mb-4 text-white text-center"><?= $title; ?></h1>
            <div class="dropdown-divider mb-3 mt-3"></div>

            <div class="row">
                <div class="col-md-5 mt-3 container-fluid">
                    <form action="<?= base_url('anggota/index') ?>" method="post">
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" placeholder="Masukkan keyword" name="keyword_buku" autocomplete="off" autofocus>
                            <div class="input-group-append">
                                <input class="btn btn-primary" type="submit" name="submit">
                            </div>
                        </div>
                    </form>
                    <?php if (empty($dataBuku)) : ?>
                        <tr>
                            <td colspan="8">
                                <div class="alert alert-danger" role="alert">
                                    Data tidak ditemukan!
                                </div>
                            </td>
                        </tr>
                    <?php endif; ?>
                    <?php if (!empty($total_rows_buku)) : ?>
                        <h1 class="h5 mb-2 text-white text-center">Results : <?= $total_rows_buku; ?></h1>
                        <?php if (($keyword_buku)) : ?>
                            <h1 class="h5 mb-2 text-white text-center">Keyword: <?= $keyword_buku; ?></h1>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>



            <!-- Content Row -->
            <div class="row container-fluid ml-2 mr-2 mt-2">

                <!-- Buku -->
                <?php foreach ($dataBuku as $bA) : ?>
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-warning shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1"><?= $bA['genre_buku']; ?></div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $bA['judul_buku']; ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-book fa-2x text-primary"></i>
                                    </div>
                                </div>
                                <div class="dropdown-divider mb-3 mt-3"></div>
                                <!-- <div class="text-center">
                                    <img src="https://cdn.myanimelist.net/images/anime/2/75533.jpg" alt="" style="width: 128px; height: 128px;">
                                </div>
                                <div class="dropdown-divider mb-3 mt-3"></div> -->
                                <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
                                <div class="flex-row mb-2">
                                    <style>
                                        .yellow-color {
                                            color: yellow;
                                        }
                                    </style>
                                    <span class="fa fa-star yellow-color"></span>
                                    <span class="fa fa-star yellow-color"></span>
                                    <span class="fa fa-star yellow-color"></span>
                                    <span class="fa fa-star yellow-color"></span>
                                    <span class="fa fa-star"></span>
                                </div>
                                <!-- <div class="dropdown mb-">
                                    <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Detail
                                    </button>
                                    <div class="dropdown-menu animated--fade-in" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="https://www.goodreads.com/book/show/52296950-just-you-and-me">Baca
                                            Selengkapnya</a>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <?= $this->pagination->create_links(); ?>


    <?php endswitch; ?>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
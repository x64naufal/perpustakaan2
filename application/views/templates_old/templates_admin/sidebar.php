<!-- Sidebar -->
        <ul class="navbar-nav sidebar" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= base_url('auth'); ?>">
                <div class="sidebar-brand-icon rotate-n-15">
                    <i class="fas fa-book-open"></i>
                </div>
                <div class="sidebar-brand-text mx-3">Perpustakaan</div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
                Administrator
            </div>

            <!-- Nav Item - Dashboard -->
            <li class="nav-item <?= $dashboardaktif; ?>">
                <a class="nav-link" href="<?= base_url('admin'); ?>">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
                Manajemen
            </div>

            <!-- Nav Item - Anggota -->
            <li class="nav-item <?= $anggotaaktif; ?>">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseAnggota"
                    aria-expanded="true" aria-controls="collapseAnggota">
                    <i class="fas fa-fw fa-users"></i>
                    <span>Data Anggota</span>
                </a>
                <div id="collapseAnggota" class="collapse <?= $anggotashow; ?>" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Anggota</h6>
                        <a class="collapse-item <?= $anggotaaktif1; ?>" href="<?= base_url('admin/dataAnggota'); ?>">Data Anggota</a>
                        <a class="collapse-item <?= $anggotaaktif2; ?>" href="<?= base_url('admin/tambahAnggota'); ?>">Tambah Anggota</a>
                    </div>
                </div>
            </li>

            <!-- Nav Item - Buku -->
            <li class="nav-item <?= $bukuaktif; ?>">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseBuku"
                    aria-expanded="true" aria-controls="collapseBuku">
                    <i class="fas fa-fw fa-book"></i>
                    <span>Data Buku</span>
                </a>
                <div id="collapseBuku" class="collapse <?= $bukushow; ?>" aria-labelledby="headingUtilities"
                    data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Buku</h6>
                        <a class="collapse-item <?= $bukuaktif1; ?>" href="<?= base_url('admin/dataBuku'); ?>">Data Buku</a>
                        <a class="collapse-item <?= $bukuaktif2; ?>" href="<?= base_url('admin/tambahBuku'); ?>">Tambah Buku</a>
                    </div>
                </div>
            </li>

            <!-- Nav Item - Transaksi -->
            <li class="nav-item <?= $transaksiaktif; ?>">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTransaksi"
                    aria-expanded="true" aria-controls="collapseTransaksi">
                    <i class="fas fa-fw fa-clipboard-list"></i>
                    <span>Transaksi Peminjaman</span>
                </a>
                <div id="collapseTransaksi" class="collapse <?= $transaksishow; ?>" aria-labelledby="headingUtilities"
                    data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Transaksi</h6>
                        <a class="collapse-item <?= $transaksiaktif1; ?>" href="<?= base_url('admin/peminjaman'); ?>">Transaksi Peminjaman</a>
                        <a class="collapse-item <?= $transaksiaktif2; ?>" href="<?= base_url('admin/pengembalian'); ?>">Transaksi Pengembalian</a>
                        <a class="collapse-item <?= $transaksiaktif3; ?>" href="<?= base_url('admin/tambahPeminjaman'); ?>">Input Transaksi Pinjam</a>
                    </div>
                </div>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
                Profil
            </div>

            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item <?= $profilaktif; ?>">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseProfil"
                    aria-expanded="true" aria-controls="collapseProfil">
                    <i class="fas fa-fw fa-user-edit"></i>
                    <span>Profil</span>
                </a>
                <div id="collapseProfil" class="collapse <?= $profilshow; ?>" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Profil</h6>
                        <a class="collapse-item <?= $profilaktif1; ?>" href="">Detail Profil</a>
                        <a class="collapse-item <?= $profilaktif2; ?>" href="">Edit Profil</a>
                    </div>
                </div>
            </li>

            <!-- Nav Item - Logout -->
            <li class="nav-item">
                <a class="nav-link" href="<?= base_url('auth/logout'); ?>" data-toggle="modal" data-target="#logoutModal">
                    <i class="fas fa-fw fa-sign-out-alt"></i>
                    <span>Logout</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

        </ul>
        <!-- End of Sidebar -->
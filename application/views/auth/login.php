    <div class="container">

        <div class="row justify-content-center m-4">
            <div class="col-lg-7">
                <div class="text-center">
                    <h1 class="h4 text-white">Selamat Datang pada Aplikasi <br>Perpustakaan SMKN 2 Banjarmasin</h1>
                </div>
            </div>
        </div>
        <!-- Outer Row -->
        <div class="row justify-content-center">
            <div class="col-lg-5">
                <div class="card o-hidden border-0 shadow-lg">

                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col-lg">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4">Halaman Login</h1>
                                    </div>
                                    <?= form_error('menu', '<div class="alert alert-danger" role="alert">', '</div>');  ?>
                                    <?php if ($this->session->flashdata('message')) : ?>
                                        <?= $this->session->flashdata('message'); ?>
                                    <?php endif; ?>
                                    <form class="user" method="post" action="<?= base_url('auth'); ?>">
                                        <div class="form-group">
                                            <input type="text" class="form-control form-control-user" id="username" name="username" placeholder="Masukkan Username..." value="<?= set_value('username'); ?>">
                                            <?= form_error('username', '<small class="text-danger ml-3">', '</small>'); ?>
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control form-control-user" id="password" name="password" placeholder="Password">
                                            <?= form_error('password', '<small class="text-danger ml-3">', '</small>'); ?>
                                        </div>
                                        <button type="submit" class="btn btn-primary btn-user btn-block">
                                            Login
                                        </button>
                                    </form>
                                    <hr>
                                    <div class="text-center">
                                        <a class="small" href="https://api.whatsapp.com/send/?phone=6287815810106&text=Admin,%20i%20want%20to%20register%20account!">Contact Admin</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>
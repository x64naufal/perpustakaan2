<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Anggota extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Admin_model');
		is_logged_in();
	}

	public function index()
	{
		$data['dashboardaktif'] = 'active';
		$data['adminaktif'] = '';
		$data['adminaktif1'] = '';
		$data['adminaktif2'] = '';
		$data['adminshow'] = '';
		$data['anggotaaktif'] = '';
		$data['anggotaaktif1'] = '';
		$data['anggotaaktif2'] = '';
		$data['anggotashow'] = '';
		$data['bukuaktif'] = '';
		$data['bukuaktif1'] = '';
		$data['bukuaktif2'] = '';
		$data['bukushow'] = '';
		$data['transaksiaktif'] = '';
		$data['transaksiaktif1'] = '';
		$data['transaksiaktif2'] = '';
		$data['transaksiaktif3'] = '';
		$data['transaksishow'] = '';
		$data['profilaktif'] = '';
		$data['profilaktif1'] = '';
		$data['profilaktif2'] = '';
		$data['profilshow'] = '';
		$data['communityaktif'] = '';

		$data['user'] = $this->db->get_where('anggota', ['fullname' => $this->session->userdata('fullname')])->row_array();
		$data['title'] = 'Halaman Anggota | Perpustakaan SMKN2 Banjarmasin';
		$data['sessionUser'] = $this->sessionci->setSession('anggota');
		// $this->sessionci->setUserdataBuku($data, '');
		// $data['dataBuku'] = $this->getci->getAllBuku();

		// $data['amountBuku'] = $this->getci->getAmountBukuById();

		// Save data search keyword
		if ($this->input->post('submit')) {
			$data['keyword_buku'] = $this->input->post('keyword_buku');
			$this->session->set_userdata('keyword_buku', $data['keyword_buku']);
		} else {
			$data['keyword_buku'] = $this->session->userdata('keyword_buku');
		}

		// Pagination config
		$this->db->like('judul_buku', $data['keyword_buku']);
		// $this->db->or_like('penulis_buku', $data['keyword_buku']);
		$this->db->from('buku');

		// $config['total_rows'] = $this->getci->getAmountBukuById();
		$config['total_rows'] = $this->db->count_all_results();
		$data['total_rows_buku'] = $config['total_rows'];
		$config['base_url'] = 'http://localhost/perpustakaan2/anggota/index/';
		$config['per_page'] = 4;
		$config['num_links'] = 5;

		// styling
		$config['full_tag_open'] = '<nav><ul class="pagination justify-content-center">';
		$config['full_tag_close'] = '</ul></nav>';

		$config['first_link'] = 'Pertama';
		$config['first_tag_open'] = '<li class="page-item">';
		$config['first_tag_close'] = '</li>';

		$config['last_link'] = 'Terakhir';
		$config['last_tag_open'] = '<li class="page-item">';
		$config['last_tag_close'] = '</li>';

		$config['next_link'] = 'Lanjut';
		$config['next_tag_open'] = '<li class="page-item">';
		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = 'Kembali';
		$config['prev_tag_open'] = '<li class="page-item">';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li class="page-item">';
		$config['num_tag_close'] = '</li>';

		$config['attributes'] = array('class' => 'page-link');

		// Initialize (konfirmasi)
		$this->pagination->initialize($config);

		// Get user dan start
		$data['startbuku'] = $this->uri->segment(3);
		$data['dataBuku'] = $this->getci->getBuku($config['per_page'], $data['startbuku'], $data['keyword_buku'], false);

		$data['listData'] = [
			'admin',
			'anggota',
			'buku'
		];

		$data['newMsg'] = $this->getci->getMessages();
		$data['newChat'] = $this->getci->getChat();
		$data['amountMsg'] = $this->getci->getAmountMessages();

		$data['setDataCom'] = [
			'PETUGAS_USER' => 'petugas',
			'ADMIN_USER' => 'admin',
			'ANGGOTA_USER' => 'anggota'
		];

		$data['bgColor'] = '#32325d';

		$this->setTemplates('user', 'index', $data);
	}

	public function setTemplates($key1, $key2, $key3)
	{

		$bg = $key3['bgColor']; //'#32325d'; //'#adb5bd';
		$key3['userColor'] = 'background-color: ' . $bg;
		$this->load->view('templates/header', $key3);
		$this->load->view('templates/sidebar', $key3);
		$this->load->view('templates/topbar', $key3);
		$this->load->view($key1 . '/' . $key2);
		$this->load->view('templates/footer');
	}

	public function databuku()
	{
		$data['dashboardaktif'] = '';
		$data['adminaktif'] = '';
		$data['adminaktif1'] = '';
		$data['adminaktif2'] = '';
		$data['adminshow'] = '';
		$data['anggotaaktif'] = '';
		$data['anggotaaktif1'] = '';
		$data['anggotaaktif2'] = '';
		$data['anggotashow'] = '';
		$data['bukuaktif'] = 'active';
		$data['bukuaktif1'] = 'active';
		$data['bukuaktif2'] = '';
		$data['bukushow'] = 'show';
		$data['transaksiaktif'] = '';
		$data['transaksiaktif1'] = '';
		$data['transaksiaktif2'] = '';
		$data['transaksiaktif3'] = '';
		$data['transaksishow'] = '';
		$data['profilaktif'] = '';
		$data['profilaktif1'] = '';
		$data['profilaktif2'] = '';
		$data['profilshow'] = '';
		$data['communityaktif'] = '';

		$data['user'] = $this->db->get_where('anggota', ['fullname' => $this->session->userdata('fullname')])->row_array();
		$data['title'] = 'Data Buku | Perpustakaan SMKN2 Banjarmasin';
		$data['sessionUser'] = $this->sessionci->setSession('anggota');
		// $this->sessionci->setUserdataBuku($data, '');

		// Save data search keyword
		if ($this->input->post('submit')) {
			$data['keyword_buku'] = $this->input->post('keyword_buku');
			$this->session->set_userdata('keyword_buku', $data['keyword_buku']);
		} else {
			$data['keyword_buku'] = $this->session->userdata('keyword_buku');
		}

		// Pagination config
		$this->db->like('judul_buku', $data['keyword_buku']);
		$this->db->or_like('penulis_buku', $data['keyword_buku']);
		$this->db->from('buku');

		// $config['total_rows'] = $this->getci->getAmountBukuById();
		$config['total_rows'] = $this->db->count_all_results();
		$data['total_rows_buku'] = $config['total_rows'];
		$config['base_url'] = 'http://localhost/perpustakaan2/anggota/dataBuku/';
		$config['per_page'] = 5;
		$config['num_links'] = 5;

		// styling
		$config['full_tag_open'] = '<nav><ul class="pagination justify-content-center">';
		$config['full_tag_close'] = '</ul></nav>';

		$config['first_link'] = 'Pertama';
		$config['first_tag_open'] = '<li class="page-item">';
		$config['first_tag_close'] = '</li>';

		$config['last_link'] = 'Terakhir';
		$config['last_tag_open'] = '<li class="page-item">';
		$config['last_tag_close'] = '</li>';

		$config['next_link'] = 'Lanjut';
		$config['next_tag_open'] = '<li class="page-item">';
		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = 'Kembali';
		$config['prev_tag_open'] = '<li class="page-item">';
		$config['prev_tag_close'] = '</li>';

		// $config['ref_link'] = 'Refresh';
		// $config['ref_tag_open'] = '<li class="page-item">';
		// $config['ref_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li class="page-item">';
		$config['num_tag_close'] = '</li>';

		$config['attributes'] = array('class' => 'page-link');

		// Initialize (konfirmasi)
		$this->pagination->initialize($config);

		// Get user dan start
		$data['startbuku'] = $this->uri->segment(3);
		$data['dataBuku'] = $this->getci->getBuku($config['per_page'], $data['startbuku'], $data['keyword_buku'], true);

		$data['setDataBuku'] = [
			'PETUGAS_USER' => 'petugas',
			'ADMIN_USER' => 'admin',
			'ANGGOTA_USER' => 'anggota'
		];

		$data['newMsg'] = $this->getci->getMessages();
		$data['newChat'] = $this->getci->getChat();
		$data['amountMsg'] = $this->getci->getAmountMessages();

		$data['setDataCom'] = [
			'PETUGAS_USER' => 'petugas',
			'ADMIN_USER' => 'admin',
			'ANGGOTA_USER' => 'anggota'
		];

		$data['bgColor'] = '#32325d';

		$this->setTemplates('user', 'dataBuku', $data);
	}

	public function community()
	{
		$data['dashboardaktif'] = '';
		$data['adminaktif'] = '';
		$data['adminaktif1'] = '';
		$data['adminaktif2'] = '';
		$data['adminshow'] = '';
		$data['anggotaaktif'] = '';
		$data['anggotaaktif1'] = '';
		$data['anggotaaktif2'] = '';
		$data['anggotashow'] = '';
		$data['bukuaktif'] = '';
		$data['bukuaktif1'] = '';
		$data['bukuaktif2'] = '';
		$data['bukushow'] = '';
		$data['transaksiaktif'] = '';
		$data['transaksiaktif1'] = '';
		$data['transaksiaktif2'] = '';
		$data['transaksiaktif3'] = '';
		$data['transaksishow'] = '';
		$data['profilaktif'] = '';
		$data['profilaktif1'] = '';
		$data['profilaktif2'] = '';
		$data['profilshow'] = '';
		$data['communityaktif'] = 'active';


		$data['newMsg'] = $this->getci->getMessages();
		$data['newChat'] = $this->getci->getChat();
		$data['amountMsg'] = $this->getci->getAmountMessages();

		$data['setDataCom'] = [
			'PETUGAS_USER' => 'petugas',
			'ADMIN_USER' => 'admin',
			'ANGGOTA_USER' => 'anggota'
		];

		$data['user'] = $this->db->get_where('anggota', ['fullname' => $this->session->userdata('fullname')])->row_array();
		$data['title'] = 'Community | Perpustakaan SMKN2 Banjarmasin';
		$data['adminAll'] = $this->getci->getAllAdmin();
		$data['sessionUser'] = $this->sessionci->setSession('anggota');

		$data['bgColor'] = '#32325d';

		$this->form_validation->set_rules('msg', 'Messages', 'required|trim');

		if ($this->form_validation->run() == false) {
			$this->setTemplates('user', 'community', $data);
		} else {
			$this->addci->addLiveChat($data['setDataCom']['ANGGOTA_USER'], $data['user']['fullname']);
			redirect('anggota/community');
		}
	}

	public function peminjaman()
	{
		$data['dashboardaktif'] = '';
		$data['adminaktif'] = '';
		$data['adminaktif1'] = '';
		$data['adminaktif2'] = '';
		$data['adminshow'] = '';
		$data['anggotaaktif'] = '';
		$data['anggotaaktif1'] = '';
		$data['anggotaaktif2'] = '';
		$data['anggotashow'] = '';
		$data['bukuaktif'] = '';
		$data['bukuaktif1'] = '';
		$data['bukuaktif2'] = '';
		$data['bukushow'] = '';
		$data['transaksiaktif'] = 'active';
		$data['transaksiaktif1'] = 'active';
		$data['transaksiaktif2'] = '';
		$data['transaksiaktif3'] = '';
		$data['transaksishow'] = 'show';
		$data['profilaktif'] = '';
		$data['profilaktif1'] = '';
		$data['profilaktif2'] = '';
		$data['profilshow'] = '';
		$data['communityaktif'] = '';

		$data['user'] = $this->db->get_where('anggota', ['fullname' => $this->session->userdata('fullname')])->row_array();
		$data['title'] = 'Data Peminjaman | Perpustakaan SMKN2 Banjarmasin';
		$data['sessionUser'] = $this->sessionci->setSession('anggota');
		// $this->sessionci->setUserdataBuku($data, '');

		// Save data search keyword
		if ($this->input->post('submit')) {
			$data['keyword_user'] = $this->input->post('keyword_user');
			$this->session->set_userdata('keyword_user', $data['keyword_user']);
		} else {
			$data['keyword_user'] = $this->session->userdata('keyword_user');
		}

		// Pagination config
		$this->db->like('nama_anggota', $data['keyword_user']);
		$this->db->or_like('judul_buku', $data['keyword_user']);
		$this->db->from('view_peminjaman2');

		// $config['total_rows'] = $this->getci->getAmountBukuById();
		$config['total_rows'] = $this->db->count_all_results();
		$data['total_rows_peminjam'] = $config['total_rows'];
		$config['base_url'] = 'http://localhost/perpustakaan2/anggota/peminjaman/';
		$config['per_page'] = 5;
		$config['num_links'] = 5;

		// styling
		$config['full_tag_open'] = '<nav><ul class="pagination justify-content-center">';
		$config['full_tag_close'] = '</ul></nav>';

		$config['first_link'] = 'Pertama';
		$config['first_tag_open'] = '<li class="page-item">';
		$config['first_tag_close'] = '</li>';

		$config['last_link'] = 'Terakhir';
		$config['last_tag_open'] = '<li class="page-item">';
		$config['last_tag_close'] = '</li>';

		$config['next_link'] = 'Lanjut';
		$config['next_tag_open'] = '<li class="page-item">';
		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = 'Kembali';
		$config['prev_tag_open'] = '<li class="page-item">';
		$config['prev_tag_close'] = '</li>';

		// $config['ref_link'] = 'Refresh';
		// $config['ref_tag_open'] = '<li class="page-item">';
		// $config['ref_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li class="page-item">';
		$config['num_tag_close'] = '</li>';

		$config['attributes'] = array('class' => 'page-link');

		// Initialize (konfirmasi)
		$this->pagination->initialize($config);

		// Get user dan start
		$data['startpeminjam'] = $this->uri->segment(3);
		$data['dataPeminjam'] = $this->getci->getPeminjam($config['per_page'], $data['startpeminjam'], $data['keyword_user'], true);
		// $data['dataPeminjamV2'] = $this->getci->getPeminjamV2();

		$data['setDataPeminjam'] = [
			'PETUGAS_USER' => 'petugas',
			'ADMIN_USER' => 'admin',
			'ANGGOTA_USER' => 'anggota'
		];

		$data['newMsg'] = $this->getci->getMessages();
		$data['newChat'] = $this->getci->getChat();
		$data['amountMsg'] = $this->getci->getAmountMessages();

		$data['setDataCom'] = [
			'PETUGAS_USER' => 'petugas',
			'ADMIN_USER' => 'admin',
			'ANGGOTA_USER' => 'anggota'
		];

		$data['bgColor'] = '#32325d';

		$this->setTemplates('user', 'dataPeminjaman', $data);
	}

	public function pengembalian()
	{
		$data['dashboardaktif'] = '';
		$data['adminaktif'] = '';
		$data['adminaktif1'] = '';
		$data['adminaktif2'] = '';
		$data['adminshow'] = '';
		$data['anggotaaktif'] = '';
		$data['anggotaaktif1'] = '';
		$data['anggotaaktif2'] = '';
		$data['anggotashow'] = '';
		$data['bukuaktif'] = '';
		$data['bukuaktif1'] = '';
		$data['bukuaktif2'] = '';
		$data['bukushow'] = '';
		$data['transaksiaktif'] = 'active';
		$data['transaksiaktif1'] = '';
		$data['transaksiaktif2'] = 'active';
		$data['transaksiaktif3'] = '';
		$data['transaksishow'] = 'show';
		$data['profilaktif'] = '';
		$data['profilaktif1'] = '';
		$data['profilaktif2'] = '';
		$data['profilshow'] = '';
		$data['communityaktif'] = '';

		$data['user'] = $this->db->get_where('anggota', ['fullname' => $this->session->userdata('fullname')])->row_array();
		$data['title'] = 'Data Pengembalian | Perpustakaan SMKN2 Banjarmasin';
		$data['sessionUser'] = $this->sessionci->setSession('anggota');
		// $this->sessionci->setUserdataBuku($data, '');

		// Save data search keyword
		if ($this->input->post('submit')) {
			$data['keyword_user'] = $this->input->post('keyword_user');
			$this->session->set_userdata('keyword_user', $data['keyword_user']);
		} else {
			$data['keyword_user'] = $this->session->userdata('keyword_user');
		}

		// Pagination config
		$this->db->like('nama_anggota', $data['keyword_user']);
		$this->db->or_like('judul_buku', $data['keyword_user']);
		$this->db->from('view_pengembalian2');

		// $config['total_rows'] = $this->getci->getAmountBukuById();
		$config['total_rows'] = $this->db->count_all_results();
		$data['total_rows_pengembalian'] = $config['total_rows'];
		$config['base_url'] = 'http://localhost/perpustakaan2/anggota/pengembalian/';
		$config['per_page'] = 5;
		$config['num_links'] = 5;

		// styling
		$config['full_tag_open'] = '<nav><ul class="pagination justify-content-center">';
		$config['full_tag_close'] = '</ul></nav>';

		$config['first_link'] = 'Pertama';
		$config['first_tag_open'] = '<li class="page-item">';
		$config['first_tag_close'] = '</li>';

		$config['last_link'] = 'Terakhir';
		$config['last_tag_open'] = '<li class="page-item">';
		$config['last_tag_close'] = '</li>';

		$config['next_link'] = 'Lanjut';
		$config['next_tag_open'] = '<li class="page-item">';
		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = 'Kembali';
		$config['prev_tag_open'] = '<li class="page-item">';
		$config['prev_tag_close'] = '</li>';

		// $config['ref_link'] = 'Refresh';
		// $config['ref_tag_open'] = '<li class="page-item">';
		// $config['ref_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li class="page-item">';
		$config['num_tag_close'] = '</li>';

		$config['attributes'] = array('class' => 'page-link');

		// Initialize (konfirmasi)
		$this->pagination->initialize($config);

		// Get user dan start
		$data['startpengembalian'] = $this->uri->segment(3);
		$data['dataPengembalian'] = $this->getci->getPengembalian($config['per_page'], $data['startpengembalian'], $data['keyword_user'], true);
		// $data['dataPeminjamV2'] = $this->getci->getPeminjamV2();

		$data['setDataPeminjam'] = [
			'PETUGAS_USER' => 'petugas',
			'ADMIN_USER' => 'admin',
			'ANGGOTA_USER' => 'anggota'
		];

		$data['newMsg'] = $this->getci->getMessages();
		$data['newChat'] = $this->getci->getChat();
		$data['amountMsg'] = $this->getci->getAmountMessages();

		$data['setDataCom'] = [
			'PETUGAS_USER' => 'petugas',
			'ADMIN_USER' => 'admin',
			'ANGGOTA_USER' => 'anggota'
		];

		$data['bgColor'] = '#32325d';

		$this->setTemplates('user', 'dataPengembalian', $data);
	}

	public function tambahPeminjaman()
	{
		$data['dashboardaktif'] = '';
		$data['adminaktif'] = '';
		$data['adminaktif1'] = '';
		$data['adminaktif2'] = '';
		$data['adminshow'] = '';
		$data['anggotaaktif'] = '';
		$data['anggotaaktif1'] = '';
		$data['anggotaaktif2'] = '';
		$data['anggotashow'] = '';
		$data['bukuaktif'] = '';
		$data['bukuaktif1'] = '';
		$data['bukuaktif2'] = '';
		$data['bukushow'] = '';
		$data['transaksiaktif'] = 'active';
		$data['transaksiaktif1'] = '';
		$data['transaksiaktif2'] = '';
		$data['transaksiaktif3'] = 'active';
		$data['transaksishow'] = 'show';
		$data['profilaktif'] = '';
		$data['profilaktif1'] = '';
		$data['profilaktif2'] = '';
		$data['profilshow'] = '';
		$data['communityaktif'] = '';


		$data['newMsg'] = $this->getci->getMessages();
		$data['newChat'] = $this->getci->getChat();
		$data['amountMsg'] = $this->getci->getAmountMessages();

		$data['setDataCom'] = [
			'PETUGAS_USER' => 'petugas',
			'ADMIN_USER' => 'admin',
			'ANGGOTA_USER' => 'anggota'
		];

		$data['user'] = $this->db->get_where('anggota', ['fullname' => $this->session->userdata('fullname')])->row_array();
		$data['listbuku'] = $this->getci->getAllBuku();
		// $data['listadmin'] = $this->getci->getAllAdmin();
		$data['title'] = 'Input Transaksi | Perpustakaan SMKN2 Banjarmasin';
		// $data['listuser'] = $this->getci->getAllUser();
		$data['listuser'] = $data['user'];
		$data['sessionUser'] = $this->sessionci->setSession('anggota');

		$data['bgColor'] = '#32325d';

		$this->form_validation->set_rules('anggota', 'Anggota', 'required|trim');
		$this->form_validation->set_rules('judul', 'Judul Buku', 'required|trim');
		$this->form_validation->set_rules('wk1', 'Waktu Peminjaman', 'required|trim');
		$this->form_validation->set_rules('wk2', 'Waktu Pengembelian', 'required|trim');

		if ($this->form_validation->run() == false) {
			$this->setTemplates('user', 'tambahPeminjaman', $data);
		} else {
			$this->addci->inputTransaksiV2('', 'anggota');
			$this->session->set_flashdata('flash', 'diajukan');
			redirect('anggota/peminjaman');
		}
	}

	public function ajukanPengembalian($id)
	{
		$data['dashboardaktif'] = '';
		$data['adminaktif'] = '';
		$data['adminaktif1'] = '';
		$data['adminaktif2'] = '';
		$data['adminshow'] = '';
		$data['anggotaaktif'] = '';
		$data['anggotaaktif1'] = '';
		$data['anggotaaktif2'] = '';
		$data['anggotashow'] = '';
		$data['bukuaktif'] = '';
		$data['bukuaktif1'] = '';
		$data['bukuaktif2'] = '';
		$data['bukushow'] = '';
		$data['transaksiaktif'] = 'active';
		$data['transaksiaktif1'] = '';
		$data['transaksiaktif2'] = '';
		$data['transaksiaktif3'] = 'active';
		$data['transaksishow'] = 'show';
		$data['profilaktif'] = '';
		$data['profilaktif1'] = '';
		$data['profilaktif2'] = '';
		$data['profilshow'] = '';
		$data['communityaktif'] = '';

		$data['user'] = $this->db->get_where('anggota', ['fullname' => $this->session->userdata('fullname')])->row_array();
		$data['title'] = 'Edit Peminjaman | Perpustakaan SMKN2 Banjarmasin';
		$data['sessionUser'] = $this->sessionci->setSession('anggota');
		$data['listbuku'] = $this->getci->getAllBuku();
		$data['peminjamans'] = $this->getci->getPeminjamanById($id);

		$data['newMsg'] = $this->getci->getMessages();
		$data['amountMsg'] = $this->getci->getAmountMessages();

		$data['setDataCom'] = [
			'PETUGAS_USER' => 'petugas',
			'ADMIN_USER' => 'admin',
			'ANGGOTA_USER' => 'anggota'
		];

		$data['bgColor'] = '#32325d';

		$this->form_validation->set_rules('anggota', 'Anggota', 'required|trim');
		$this->form_validation->set_rules('judul', 'Judul Buku', 'required|trim');
		$this->form_validation->set_rules('wk1', 'Waktu Peminjaman', 'required|trim');
		$this->form_validation->set_rules('wk2', 'Waktu Pengembelian', 'required|trim');
		$this->form_validation->set_rules('denda', 'Denda', 'required|trim');

		if ($this->form_validation->run() == false) {
			$this->setTemplates('user', 'ajukanPengembalian', $data);
			// var_dump($data['peminjamans']['id_peminjaman']);
		} else {
			// $this->editci->inputPengembalian();
			// $ids = $data['peminjamans']['id_peminjaman'];
			// $this->delci->hapusPeminjaman($ids);
			$this->addci->inputPengembalianV2('', $data['user']['id_anggota'], 'anggota');
			$this->session->set_flashdata('flash', 'diajukan');
			redirect('anggota/pengembalian');
		}
	}
}

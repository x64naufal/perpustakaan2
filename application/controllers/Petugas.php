<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Petugas extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Admin_model');
		$this->load->dbforge();
		is_logged_in();
	}

	public function index()
	{
		$data['dashboardaktif'] = 'active';
		$data['adminaktif'] = '';
		$data['adminaktif1'] = '';
		$data['adminaktif2'] = '';
		$data['adminshow'] = '';
		$data['anggotaaktif'] = '';
		$data['anggotaaktif1'] = '';
		$data['anggotaaktif2'] = '';
		$data['anggotashow'] = '';
		$data['bukuaktif'] = '';
		$data['bukuaktif1'] = '';
		$data['bukuaktif2'] = '';
		$data['bukushow'] = '';
		$data['transaksiaktif'] = '';
		$data['transaksiaktif1'] = '';
		$data['transaksiaktif2'] = '';
		$data['transaksiaktif3'] = '';
		$data['transaksishow'] = '';
		$data['profilaktif'] = '';
		$data['profilaktif1'] = '';
		$data['profilaktif2'] = '';
		$data['profilshow'] = '';
		$data['communityaktif'] = '';

		$data['user'] = $this->db->get_where('petugas', ['fullname' => $this->session->userdata('fullname')])->row_array();
		$data['title'] = 'Halaman petugas | Perpustakaan SMKN2 Banjarmasin';
		$data['sessionUser'] = $this->sessionci->setSession('petugas');
		$data['newMsg'] = $this->getci->getMessages();

		$data['amountAdmin'] = $this->getci->getAmountAdminById();
		$data['amountAnggota'] = $this->getci->getAmountAnggotaById();
		$data['amountBuku'] = $this->getci->getAmountBukuById();
		$data['amountMsg'] = $this->getci->getAmountMessages();

		// $data['userPetugas'] = $this->getci->getPetugasById(0);

		$data['listData'] = [
			'admin',
			'anggota',
			'buku'
		];

		$data['listData2'] = [
			'admin' => $this->getci->getAmountAdminById(),
			'anggota' => $this->getci->getAmountAnggotaById(),
			'buku' => $this->getci->getAmountBukuById()
		];

		$data['setDataDashboard'] = [
			'PETUGAS_USER' => 'petugas',
			'ADMIN_USER' => 'admin'
		];

		$data['setDataCom'] = [
			'PETUGAS_USER' => 'petugas',
			'ADMIN_USER' => 'admin',
			'ANGGOTA_USER' => 'anggota'
		];

		$data['bgColor'] = '#32325d';

		$this->setTemplates('user', 'index', $data);
	}

	public function setTemplates($key1, $key2, $key3)
	{

		$bg = $this->getci->getColor($key3['user']['color']); //$key3['bgColor']; //'#32325d'; //'#adb5bd';
		$key3['userColor'] = 'background-color: ' . $bg;
		// $this->getci->getColor($key3['user']['color']);
		$this->load->view('templates/header', $key3);
		$this->load->view('templates/sidebar', $key3);
		$this->load->view('templates/topbar', $key3);
		$this->load->view($key1 . '/' . $key2);
		$this->load->view('templates/footer');
	}

	public function dataadmin()
	{
		$data['dashboardaktif'] = '';
		$data['adminaktif'] = 'active';
		$data['adminaktif1'] = 'active';
		$data['adminaktif2'] = '';
		$data['adminshow'] = 'show';
		$data['anggotaaktif'] = '';
		$data['anggotaaktif1'] = '';
		$data['anggotaaktif2'] = '';
		$data['anggotashow'] = '';
		$data['bukuaktif'] = '';
		$data['bukuaktif1'] = '';
		$data['bukuaktif2'] = '';
		$data['bukushow'] = '';
		$data['transaksiaktif'] = '';
		$data['transaksiaktif1'] = '';
		$data['transaksiaktif2'] = '';
		$data['transaksiaktif3'] = '';
		$data['transaksishow'] = '';
		$data['profilaktif'] = '';
		$data['profilaktif1'] = '';
		$data['profilaktif2'] = '';
		$data['profilshow'] = '';
		$data['communityaktif'] = '';

		$data['user'] = $this->db->get_where('petugas', ['fullname' => $this->session->userdata('fullname')])->row_array();
		$data['title'] = 'Data Admin | Perpustakaan SMKN2 Banjarmasin';
		$data['sessionUser'] = $this->sessionci->setSession('petugas');

		$data['newMsg'] = $this->getci->getMessages();
		$data['amountMsg'] = $this->getci->getAmountMessages();

		// $data['adminAll'] = $this->getci->getAllAdmin();

		// Save data search keyword
		if ($this->input->post('submit')) {
			$data['keyword_admin'] = $this->input->post('keyword_admin');
			$this->session->set_userdata('keyword_admin', $data['keyword_admin']);
		} else {
			$data['keyword_admin'] = $this->session->userdata('keyword_admin');
		}

		// Pagination config
		$this->db->like('fullname', $data['keyword_admin']);
		$this->db->or_like('username', $data['keyword_admin']);
		$this->db->from('admin');

		// $config['total_rows'] = $this->getci->getAmountBukuById();
		$config['total_rows'] = $this->db->count_all_results();
		$data['total_rows_admin'] = $config['total_rows'];
		$config['base_url'] = 'http://localhost/perpustakaan2/petugas/dataAdmin/';
		$config['per_page'] = 5;
		$config['num_links'] = 5;

		// styling
		$config['full_tag_open'] = '<nav><ul class="pagination justify-content-center">';
		$config['full_tag_close'] = '</ul></nav>';

		$config['first_link'] = 'Pertama';
		$config['first_tag_open'] = '<li class="page-item">';
		$config['first_tag_close'] = '</li>';

		$config['last_link'] = 'Terakhir';
		$config['last_tag_open'] = '<li class="page-item">';
		$config['last_tag_close'] = '</li>';

		$config['next_link'] = 'Lanjut';
		$config['next_tag_open'] = '<li class="page-item">';
		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = 'Kembali';
		$config['prev_tag_open'] = '<li class="page-item">';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li class="page-item">';
		$config['num_tag_close'] = '</li>';

		$config['attributes'] = array('class' => 'page-link');

		// Initialize (konfirmasi)
		$this->pagination->initialize($config);

		// Get user dan start
		$data['startadmin'] = $this->uri->segment(3);
		$data['dataAdmin'] = $this->getci->getAdmin($config['per_page'], $data['startadmin'], $data['keyword_admin']);


		$data['setDataAdmin'] = [
			'PETUGAS_USER' => 'petugas'
		];

		$data['setDataCom'] = [
			'PETUGAS_USER' => 'petugas',
			'ADMIN_USER' => 'admin',
			'ANGGOTA_USER' => 'anggota'
		];

		$data['bgColor'] = '#32325d';

		$this->setTemplates('user', 'dataAdmin', $data);
	}

	public function dataanggota()
	{
		$data['dashboardaktif'] = '';
		$data['adminaktif'] = '';
		$data['adminaktif1'] = '';
		$data['adminaktif2'] = '';
		$data['adminshow'] = '';
		$data['anggotaaktif'] = 'active';
		$data['anggotaaktif1'] = 'active';
		$data['anggotaaktif2'] = '';
		$data['anggotashow'] = 'show';
		$data['bukuaktif'] = '';
		$data['bukuaktif1'] = '';
		$data['bukuaktif2'] = '';
		$data['bukushow'] = '';
		$data['transaksiaktif'] = '';
		$data['transaksiaktif1'] = '';
		$data['transaksiaktif2'] = '';
		$data['transaksiaktif3'] = '';
		$data['transaksishow'] = '';
		$data['profilaktif'] = '';
		$data['profilaktif1'] = '';
		$data['profilaktif2'] = '';
		$data['profilshow'] = '';
		$data['communityaktif'] = '';

		$data['user'] = $this->db->get_where('petugas', ['fullname' => $this->session->userdata('fullname')])->row_array();
		$data['title'] = 'Data Anggota | Perpustakaan SMKN2 Banjarmasin';
		$data['sessionUser'] = $this->sessionci->setSession('petugas');

		$data['newMsg'] = $this->getci->getMessages();
		$data['amountMsg'] = $this->getci->getAmountMessages();

		$data['setDataCom'] = [
			'PETUGAS_USER' => 'petugas',
			'ADMIN_USER' => 'admin',
			'ANGGOTA_USER' => 'anggota'
		];

		// Save data search keyword
		if ($this->input->post('submit')) {
			$data['keyword_anggota'] = $this->input->post('keyword_anggota');
			$this->session->set_userdata('keyword_anggota', $data['keyword_anggota']);
		} else {
			$data['keyword_anggota'] = $this->session->userdata('keyword_anggota');
		}

		// Pagination config
		$this->db->like('fullname', $data['keyword_anggota']);
		$this->db->or_like('kelas_anggota', $data['keyword_anggota']);
		$this->db->from('anggota');

		// $config['total_rows'] = $this->getci->getAmountBukuById();
		$config['total_rows'] = $this->db->count_all_results();
		$data['total_rows_anggota'] = $config['total_rows'];
		$config['base_url'] = 'http://localhost/perpustakaan2/petugas/dataAnggota/';
		$config['per_page'] = 5;
		$config['num_links'] = 5;

		// styling
		$config['full_tag_open'] = '<nav><ul class="pagination justify-content-center">';
		$config['full_tag_close'] = '</ul></nav>';

		$config['first_link'] = 'Pertama';
		$config['first_tag_open'] = '<li class="page-item">';
		$config['first_tag_close'] = '</li>';

		$config['last_link'] = 'Terakhir';
		$config['last_tag_open'] = '<li class="page-item">';
		$config['last_tag_close'] = '</li>';

		$config['next_link'] = 'Lanjut';
		$config['next_tag_open'] = '<li class="page-item">';
		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = 'Kembali';
		$config['prev_tag_open'] = '<li class="page-item">';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li class="page-item">';
		$config['num_tag_close'] = '</li>';

		$config['attributes'] = array('class' => 'page-link');

		// Initialize (konfirmasi)
		$this->pagination->initialize($config);

		// Get user dan start
		$data['startanggota'] = $this->uri->segment(3);
		$data['dataAnggota'] = $this->getci->getAnggota($config['per_page'], $data['startanggota'], $data['keyword_anggota']);

		$data['setDataCom'] = [
			'PETUGAS_USER' => 'petugas',
			'ADMIN_USER' => 'admin',
			'ANGGOTA_USER' => 'anggota'
		];

		$data['setDataAnggota'] = [
			'PETUGAS_USER' => 'petugas',
			'ADMIN_USER' => 'admin'
		];

		$data['bgColor'] = '#32325d';

		$this->setTemplates('user', 'dataAnggota', $data);
	}

	public function databuku()
	{
		$data['dashboardaktif'] = '';
		$data['adminaktif'] = '';
		$data['adminaktif1'] = '';
		$data['adminaktif2'] = '';
		$data['adminshow'] = '';
		$data['anggotaaktif'] = '';
		$data['anggotaaktif1'] = '';
		$data['anggotaaktif2'] = '';
		$data['anggotashow'] = '';
		$data['bukuaktif'] = 'active';
		$data['bukuaktif1'] = 'active';
		$data['bukuaktif2'] = '';
		$data['bukushow'] = 'show';
		$data['transaksiaktif'] = '';
		$data['transaksiaktif1'] = '';
		$data['transaksiaktif2'] = '';
		$data['transaksiaktif3'] = '';
		$data['transaksishow'] = '';
		$data['profilaktif'] = '';
		$data['profilaktif1'] = '';
		$data['profilaktif2'] = '';
		$data['profilshow'] = '';
		$data['communityaktif'] = '';

		$data['user'] = $this->db->get_where('petugas', ['fullname' => $this->session->userdata('fullname')])->row_array();
		$data['title'] = 'Data Buku | Perpustakaan SMKN2 Banjarmasin';
		$data['sessionUser'] = $this->sessionci->setSession('petugas');

		$data['newMsg'] = $this->getci->getMessages();
		$data['amountMsg'] = $this->getci->getAmountMessages();

		$this->sessionci->setUserdataBuku($data, '');

		// Save data search keyword
		if ($this->input->post('submit')) {
			$data['keyword_buku'] = $this->input->post('keyword_buku');
			$this->session->set_userdata('keyword_buku', $data['keyword_buku']);
		} else {
			$data['keyword_buku'] = $this->session->userdata('keyword_buku');
		}

		// Pagination config
		$this->db->like('judul_buku', $data['keyword_buku']);
		$this->db->or_like('penulis_buku', $data['keyword_buku']);
		$this->db->from('buku');

		// $config['total_rows'] = $this->getci->getAmountBukuById();
		$config['total_rows'] = $this->db->count_all_results();
		$data['total_rows_buku'] = $config['total_rows'];
		$config['base_url'] = 'http://localhost/perpustakaan2/petugas/dataBuku/';
		$config['per_page'] = 5;
		$config['num_links'] = 5;

		// styling
		$config['full_tag_open'] = '<nav><ul class="pagination justify-content-center">';
		$config['full_tag_close'] = '</ul></nav>';

		$config['first_link'] = 'Pertama';
		$config['first_tag_open'] = '<li class="page-item">';
		$config['first_tag_close'] = '</li>';

		$config['last_link'] = 'Terakhir';
		$config['last_tag_open'] = '<li class="page-item">';
		$config['last_tag_close'] = '</li>';

		$config['next_link'] = 'Lanjut';
		$config['next_tag_open'] = '<li class="page-item">';
		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = 'Kembali';
		$config['prev_tag_open'] = '<li class="page-item">';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li class="page-item">';
		$config['num_tag_close'] = '</li>';

		$config['attributes'] = array('class' => 'page-link');

		// Initialize (konfirmasi)
		$this->pagination->initialize($config);

		// Get user dan start
		$data['startbuku'] = $this->uri->segment(3);
		$data['dataBuku'] = $this->getci->getBuku($config['per_page'], $data['startbuku'], $data['keyword_buku'], true);

		$data['setDataBuku'] = [
			'PETUGAS_USER' => 'petugas',
			'ADMIN_USER' => 'admin',
			'ANGGOTA_USER' => 'anggota'
		];

		$data['setDataCom'] = [
			'PETUGAS_USER' => 'petugas',
			'ADMIN_USER' => 'admin',
			'ANGGOTA_USER' => 'anggota'
		];

		$data['bgColor'] = '#32325d';

		$this->setTemplates('user', 'dataBuku', $data);
	}

	// public function tambahLiveChat()
	// {
	// 	$data['dashboardaktif'] = '';
	// 	$data['adminaktif'] = '';
	// 	$data['adminaktif1'] = '';
	// 	$data['adminaktif2'] = '';
	// 	$data['adminshow'] = '';
	// 	$data['anggotaaktif'] = '';
	// 	$data['anggotaaktif1'] = '';
	// 	$data['anggotaaktif2'] = '';
	// 	$data['anggotashow'] = '';
	// 	$data['bukuaktif'] = '';
	// 	$data['bukuaktif1'] = '';
	// 	$data['bukuaktif2'] = '';
	// 	$data['bukushow'] = '';
	// 	$data['transaksiaktif'] = '';
	// 	$data['transaksiaktif1'] = '';
	// 	$data['transaksiaktif2'] = '';
	// 	$data['transaksiaktif3'] = '';
	// 	$data['transaksishow'] = '';
	// 	$data['profilaktif'] = '';
	// 	$data['profilaktif1'] = '';
	// 	$data['profilaktif2'] = '';
	// 	$data['profilshow'] = '';

	// 	$data['user'] = $this->db->get_where('petugas', ['fullname' => $this->session->userdata('fullname')])->row_array();
	// 	$data['title'] = 'Tambah Admin | Perpustakaan SMKN2 Banjarmasin';
	// 	$data['adminAll'] = $this->getci->getAllAdmin();
	// 	$data['sessionUser'] = $this->sessionci->setSession('petugas');

	// 	$data['newMsg'] = $this->getci->getMessages();
	// 	$data['amountMsg'] = $this->getci->getAmountMessages();

	// 	$data['setDataCom'] = [
	// 		'PETUGAS_USER' => 'petugas'
	// 	];

	// 	$this->form_validation->set_rules('msg', 'Type message..', 'required|trim');

	// 	if ($this->form_validation->run() == false) {
	// 		$this->setTemplates('user', 'community', $data);
	// 	} else {
	// 		$this->addci->addLiveChat($data['setDataCom']['PETUGAS_USER'], $data['user']['fullname']);
	// 		redirect('petugas/community');
	// 	}
	// }

	public function tambahAdmin()
	{
		$data['dashboardaktif'] = '';
		$data['adminaktif'] = 'active';
		$data['adminaktif1'] = '';
		$data['adminaktif2'] = 'active';
		$data['adminshow'] = 'show';
		$data['anggotaaktif'] = '';
		$data['anggotaaktif1'] = '';
		$data['anggotaaktif2'] = '';
		$data['anggotashow'] = '';
		$data['bukuaktif'] = '';
		$data['bukuaktif1'] = '';
		$data['bukuaktif2'] = '';
		$data['bukushow'] = '';
		$data['transaksiaktif'] = '';
		$data['transaksiaktif1'] = '';
		$data['transaksiaktif2'] = '';
		$data['transaksiaktif3'] = '';
		$data['transaksishow'] = '';
		$data['profilaktif'] = '';
		$data['profilaktif1'] = '';
		$data['profilaktif2'] = '';
		$data['profilshow'] = '';
		$data['communityaktif'] = '';

		$data['user'] = $this->db->get_where('petugas', ['fullname' => $this->session->userdata('fullname')])->row_array();
		$data['title'] = 'Tambah Admin | Perpustakaan SMKN2 Banjarmasin';
		$data['adminAll'] = $this->getci->getAllAdmin();
		$data['sessionUser'] = $this->sessionci->setSession('petugas');

		$data['newMsg'] = $this->getci->getMessages();
		$data['amountMsg'] = $this->getci->getAmountMessages();

		$data['setDataCom'] = [
			'PETUGAS_USER' => 'petugas',
			'ADMIN_USER' => 'admin',
			'ANGGOTA_USER' => 'anggota'
		];

		$data['bgColor'] = '#0ee3d7';

		$this->form_validation->set_rules('username', 'Username', 'required|trim');
		$this->form_validation->set_rules('password', 'Password', 'required|trim');
		$this->form_validation->set_rules('nama', 'Nama', 'required|trim');

		if ($this->form_validation->run() == false) {
			$this->setTemplates('user', 'tambahAdmin', $data);
		} else {
			$this->addci->addAdmin();
			$this->session->set_flashdata('flash', 'ditambahkan');
			redirect('petugas/dataAdmin');
		}
	}

	public function tambahAnggota()
	{
		$data['dashboardaktif'] = '';
		$data['adminaktif'] = '';
		$data['adminaktif1'] = '';
		$data['adminaktif2'] = '';
		$data['adminshow'] = '';
		$data['anggotaaktif'] = 'active';
		$data['anggotaaktif1'] = '';
		$data['anggotaaktif2'] = 'active';
		$data['anggotashow'] = 'show';
		$data['bukuaktif'] = '';
		$data['bukuaktif1'] = '';
		$data['bukuaktif2'] = '';
		$data['bukushow'] = '';
		$data['transaksiaktif'] = '';
		$data['transaksiaktif1'] = '';
		$data['transaksiaktif2'] = '';
		$data['transaksiaktif3'] = '';
		$data['transaksishow'] = '';
		$data['profilaktif'] = '';
		$data['profilaktif1'] = '';
		$data['profilaktif2'] = '';
		$data['profilshow'] = '';
		$data['communityaktif'] = '';

		$data['user'] = $this->db->get_where('petugas', ['fullname' => $this->session->userdata('fullname')])->row_array();
		$data['title'] = 'Tambah Anggota | Perpustakaan SMKN2 Banjarmasin';
		$data['userAll'] = $this->getci->getAllUser();
		$data['sessionUser'] = $this->sessionci->setSession('petugas');

		$data['newMsg'] = $this->getci->getMessages();
		$data['amountMsg'] = $this->getci->getAmountMessages();

		$data['setDataCom'] = [
			'PETUGAS_USER' => 'petugas',
			'ADMIN_USER' => 'admin',
			'ANGGOTA_USER' => 'anggota'
		];

		$data['bgColor'] = '#32325d';

		$this->form_validation->set_rules('username', 'Username', 'required|trim');
		$this->form_validation->set_rules('password', 'Password', 'required|trim');
		$this->form_validation->set_rules('nama', 'Nama', 'required|trim');
		$this->form_validation->set_rules('jk', 'Jenis Kelamin', 'required|trim');
		$this->form_validation->set_rules('kelas', 'Kelas', 'required|trim');
		$this->form_validation->set_rules('jurusan', 'Jurusan', 'required|trim');
		$this->form_validation->set_rules('no_telp', 'No Telepon', 'required|trim');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required|trim');

		if ($this->form_validation->run() == false) {
			$this->setTemplates('user', 'tambahAnggota', $data);
		} else {
			$this->addci->addAnggota();
			$this->session->set_flashdata('flash', 'ditambahkan');
			redirect('petugas/dataAnggota');
		}
	}

	public function tambahBuku()
	{
		$data['dashboardaktif'] = '';
		$data['adminaktif'] = '';
		$data['adminaktif1'] = '';
		$data['adminaktif2'] = '';
		$data['adminshow'] = '';
		$data['anggotaaktif'] = '';
		$data['anggotaaktif1'] = '';
		$data['anggotaaktif2'] = '';
		$data['anggotashow'] = '';
		$data['bukuaktif'] = 'active';
		$data['bukuaktif1'] = '';
		$data['bukuaktif2'] = 'active';
		$data['bukushow'] = 'show';
		$data['transaksiaktif'] = '';
		$data['transaksiaktif1'] = '';
		$data['transaksiaktif2'] = '';
		$data['transaksiaktif3'] = '';
		$data['transaksishow'] = '';
		$data['profilaktif'] = '';
		$data['profilaktif1'] = '';
		$data['profilaktif2'] = '';
		$data['profilshow'] = '';
		$data['communityaktif'] = '';

		$data['user'] = $this->db->get_where('petugas', ['fullname' => $this->session->userdata('fullname')])->row_array();
		$data['title'] = 'Tambah Buku | Perpustakaan SMKN2 Banjarmasin';
		$data['bukuAll'] = $this->getci->getAllBuku();
		$data['sessionUser'] = $this->sessionci->setSession('petugas');

		$data['newMsg'] = $this->getci->getMessages();
		$data['amountMsg'] = $this->getci->getAmountMessages();

		$data['setDataCom'] = [
			'PETUGAS_USER' => 'petugas',
			'ADMIN_USER' => 'admin',
			'ANGGOTA_USER' => 'anggota'
		];

		$data['bgColor'] = '#87d2ff';

		$this->form_validation->set_rules('judul', 'Judul', 'required|trim');
		$this->form_validation->set_rules('genre', 'Genre', 'required|trim');
		$this->form_validation->set_rules('penulis', 'Penulis', 'required|trim');
		$this->form_validation->set_rules('penerbit', 'Penerbit', 'required|trim');
		$this->form_validation->set_rules('tahun', 'Tahun', 'required|trim');
		$this->form_validation->set_rules('stok', 'Stok', 'required|trim');

		if ($this->form_validation->run() == false) {
			$this->setTemplates('user', 'tambahBuku', $data);
		} else {
			$this->addci->addBuku();
			$this->session->set_flashdata('flash', 'ditambahkan');
			redirect('petugas/dataBuku');
		}
	}

	public function editAdmin($id)
	{
		$data['dashboardaktif'] = '';
		$data['adminaktif'] = 'active';
		$data['adminaktif1'] = 'active';
		$data['adminaktif2'] = '';
		$data['adminshow'] = 'show';
		$data['anggotaaktif'] = '';
		$data['anggotaaktif1'] = '';
		$data['anggotaaktif2'] = '';
		$data['anggotashow'] = '';
		$data['bukuaktif'] = '';
		$data['bukuaktif1'] = '';
		$data['bukuaktif2'] = '';
		$data['bukushow'] = '';
		$data['transaksiaktif'] = '';
		$data['transaksiaktif1'] = '';
		$data['transaksiaktif2'] = '';
		$data['transaksiaktif3'] = '';
		$data['transaksishow'] = '';
		$data['profilaktif'] = '';
		$data['profilaktif1'] = '';
		$data['profilaktif2'] = '';
		$data['profilshow'] = '';
		$data['communityaktif'] = '';

		$data['user'] = $this->db->get_where('petugas', ['fullname' => $this->session->userdata('fullname')])->row_array();
		$data['title'] = 'Edit Anggota | Perpustakaan SMKN2 Banjarmasin';
		$data['sessionUser'] = $this->sessionci->setSession('petugas');

		$data['userAdmin'] = $this->getci->getAdminById($id);
		$data['adminAll'] = $this->getci->getAllAdmin();

		$data['newMsg'] = $this->getci->getMessages();
		$data['amountMsg'] = $this->getci->getAmountMessages();

		$data['setDataCom'] = [
			'PETUGAS_USER' => 'petugas',
			'ADMIN_USER' => 'admin',
			'ANGGOTA_USER' => 'anggota'
		];

		$data['bgColor'] = '#32325d';

		$this->form_validation->set_rules('username', 'Username', 'required|trim');
		$this->form_validation->set_rules('nama', 'Nama', 'required|trim');

		if ($this->form_validation->run() == false) {
			$this->setTemplates('user', 'editAdmin', $data);
		} else {
			$this->editci->editAdmin();
			$this->session->set_flashdata('flash', 'diedit');
			redirect('petugas/dataAdmin');
		}
	}

	public function editAnggota($id)
	{
		$data['dashboardaktif'] = '';
		$data['adminaktif'] = '';
		$data['adminaktif1'] = '';
		$data['adminaktif2'] = '';
		$data['adminshow'] = '';
		$data['anggotaaktif'] = 'active';
		$data['anggotaaktif1'] = 'active';
		$data['anggotaaktif2'] = '';
		$data['anggotashow'] = 'show';
		$data['bukuaktif'] = '';
		$data['bukuaktif1'] = '';
		$data['bukuaktif2'] = '';
		$data['bukushow'] = '';
		$data['transaksiaktif'] = '';
		$data['transaksiaktif1'] = '';
		$data['transaksiaktif2'] = '';
		$data['transaksiaktif3'] = '';
		$data['transaksishow'] = '';
		$data['profilaktif'] = '';
		$data['profilaktif1'] = '';
		$data['profilaktif2'] = '';
		$data['profilshow'] = '';
		$data['communityaktif'] = '';

		$data['user'] = $this->db->get_where('petugas', ['fullname' => $this->session->userdata('fullname')])->row_array();
		$data['title'] = 'Edit Anggota | Perpustakaan SMKN2 Banjarmasin';
		$data['sessionUser'] = $this->sessionci->setSession('petugas');

		$data['userAnggota'] = $this->getci->getAnggotaById($id);
		$data['userAll'] = $this->getci->getAllUser();

		$data['newMsg'] = $this->getci->getMessages();
		$data['amountMsg'] = $this->getci->getAmountMessages();

		$data['setDataCom'] = [
			'PETUGAS_USER' => 'petugas',
			'ADMIN_USER' => 'admin',
			'ANGGOTA_USER' => 'anggota'
		];

		$data['bgColor'] = '#32325d';

		$this->form_validation->set_rules('username', 'Username', 'required|trim');
		$this->form_validation->set_rules('nama', 'Nama', 'required|trim');
		$this->form_validation->set_rules('jk', 'Jenis Kelamin', 'required|trim');
		$this->form_validation->set_rules('kelas', 'Kelas', 'required|trim');
		$this->form_validation->set_rules('jurusan', 'Jurusan', 'required|trim');
		$this->form_validation->set_rules('no_telp', 'No Telepon', 'required|trim');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required|trim');

		if ($this->form_validation->run() == false) {
			$this->setTemplates('user', 'editAnggota', $data);
		} else {
			$this->editci->editAnggota();
			$this->session->set_flashdata('flash', 'diedit');
			redirect('petugas/dataAnggota');
		}
	}

	public function editBuku($id)
	{
		$data['dashboardaktif'] = '';
		$data['adminaktif'] = '';
		$data['adminaktif1'] = '';
		$data['adminaktif2'] = '';
		$data['adminshow'] = '';
		$data['anggotaaktif'] = '';
		$data['anggotaaktif1'] = '';
		$data['anggotaaktif2'] = '';
		$data['anggotashow'] = '';
		$data['bukuaktif'] = 'active';
		$data['bukuaktif1'] = 'active';
		$data['bukuaktif2'] = '';
		$data['bukushow'] = 'show';
		$data['transaksiaktif'] = '';
		$data['transaksiaktif1'] = '';
		$data['transaksiaktif2'] = '';
		$data['transaksiaktif3'] = '';
		$data['transaksishow'] = '';
		$data['profilaktif'] = '';
		$data['profilaktif1'] = '';
		$data['profilaktif2'] = '';
		$data['profilshow'] = '';
		$data['communityaktif'] = '';

		$data['user'] = $this->db->get_where('petugas', ['fullname' => $this->session->userdata('fullname')])->row_array();
		$data['title'] = 'Edit Buku | Perpustakaan SMKN2 Banjarmasin';
		$data['sessionUser'] = $this->sessionci->setSession('petugas');

		$data['listBuku'] = $this->getci->getBukuById($id);
		$data['bukuAll'] = $this->getci->getAllBuku();

		$data['newMsg'] = $this->getci->getMessages();
		$data['amountMsg'] = $this->getci->getAmountMessages();

		$data['setDataCom'] = [
			'PETUGAS_USER' => 'petugas',
			'ADMIN_USER' => 'admin',
			'ANGGOTA_USER' => 'anggota'
		];

		$data['bgColor'] = '#32325d';

		$this->form_validation->set_rules('judul', 'Judul', 'required|trim');
		$this->form_validation->set_rules('genre', 'Genre', 'required|trim');
		$this->form_validation->set_rules('penulis', 'Penulis', 'required|trim');
		$this->form_validation->set_rules('penerbit', 'Penerbit', 'required|trim');
		$this->form_validation->set_rules('tahun', 'Tahun', 'required|trim');
		$this->form_validation->set_rules('stok', 'Stok', 'required|trim');

		if ($this->form_validation->run() == false) {
			$this->setTemplates('user', 'editBuku', $data);
		} else {
			$this->editci->editBuku();
			$this->session->set_flashdata('flash', 'diedit');
			redirect('petugas/dataBuku');
		}
	}

	public function hapusAdmin($id)
	{
		$this->delci->hapusAdmin($id);
		$this->session->set_flashdata('flash', 'dihapus');
		redirect('petugas/dataAdmin');
	}

	public function hapusAnggota($id)
	{
		$this->delci->hapusAnggota($id);
		$this->session->set_flashdata('flash', 'dihapus');
		redirect('petugas/dataAnggota');
	}

	public function hapusBuku($id)
	{
		$this->delci->hapusBuku($id);
		$this->session->set_flashdata('flash', 'dihapus');
		redirect('petugas/dataBuku');
	}

	public function community()
	{
		$data['dashboardaktif'] = '';
		$data['adminaktif'] = '';
		$data['adminaktif1'] = '';
		$data['adminaktif2'] = '';
		$data['adminshow'] = '';
		$data['anggotaaktif'] = '';
		$data['anggotaaktif1'] = '';
		$data['anggotaaktif2'] = '';
		$data['anggotashow'] = '';
		$data['bukuaktif'] = '';
		$data['bukuaktif1'] = '';
		$data['bukuaktif2'] = '';
		$data['bukushow'] = '';
		$data['transaksiaktif'] = '';
		$data['transaksiaktif1'] = '';
		$data['transaksiaktif2'] = '';
		$data['transaksiaktif3'] = '';
		$data['transaksishow'] = '';
		$data['profilaktif'] = '';
		$data['profilaktif1'] = '';
		$data['profilaktif2'] = '';
		$data['profilshow'] = '';
		$data['communityaktif'] = 'active';

		$data['newMsg'] = $this->getci->getMessages();
		$data['newChat'] = $this->getci->getChat();
		$data['amountMsg'] = $this->getci->getAmountMessages();

		$data['setDataCom'] = [
			'PETUGAS_USER' => 'petugas',
			'ADMIN_USER' => 'admin',
			'ANGGOTA_USER' => 'anggota'
		];

		$data['user'] = $this->db->get_where('petugas', ['fullname' => $this->session->userdata('fullname')])->row_array();
		$data['title'] = 'Community | Perpustakaan SMKN2 Banjarmasin';
		$data['adminAll'] = $this->getci->getAllAdmin();
		$data['sessionUser'] = $this->sessionci->setSession('petugas');

		$data['bgColor'] = '#32325d';

		$this->form_validation->set_rules('msg', 'Messages', 'required|trim');

		if ($this->form_validation->run() == false) {
			$this->setTemplates('user', 'community', $data);
		} else {
			$this->addci->addLiveChat($data['setDataCom']['PETUGAS_USER'], $data['user']['fullname']);
			redirect('petugas/community');
		}
	}

	public function peminjaman()
	{
		$data['dashboardaktif'] = '';
		$data['adminaktif'] = '';
		$data['adminaktif1'] = '';
		$data['adminaktif2'] = '';
		$data['adminshow'] = '';
		$data['anggotaaktif'] = '';
		$data['anggotaaktif1'] = '';
		$data['anggotaaktif2'] = '';
		$data['anggotashow'] = '';
		$data['bukuaktif'] = '';
		$data['bukuaktif1'] = '';
		$data['bukuaktif2'] = '';
		$data['bukushow'] = '';
		$data['transaksiaktif'] = 'active';
		$data['transaksiaktif1'] = 'active';
		$data['transaksiaktif2'] = '';
		$data['transaksiaktif3'] = '';
		$data['transaksishow'] = 'show';
		$data['profilaktif'] = '';
		$data['profilaktif1'] = '';
		$data['profilaktif2'] = '';
		$data['profilshow'] = '';
		$data['communityaktif'] = '';

		$data['user'] = $this->db->get_where('petugas', ['fullname' => $this->session->userdata('fullname')])->row_array();
		$data['title'] = 'Data Peminjaman | Perpustakaan SMKN2 Banjarmasin';
		$data['sessionUser'] = $this->sessionci->setSession('petugas');
		// $this->sessionci->setUserdataBuku($data, '');

		// Save data search keyword
		if ($this->input->post('submit')) {
			$data['keyword_user'] = $this->input->post('keyword_user');
			$this->session->set_userdata('keyword_user', $data['keyword_user']);
		} else {
			$data['keyword_user'] = $this->session->userdata('keyword_user');
		}

		// Pagination config
		$this->db->like('nama_anggota', $data['keyword_user']);
		$this->db->or_like('judul_buku', $data['keyword_user']);
		$this->db->from('view_peminjaman2');

		// $config['total_rows'] = $this->getci->getAmountBukuById();
		$config['total_rows'] = $this->db->count_all_results();
		$data['total_rows_peminjam'] = $config['total_rows'];
		$config['base_url'] = 'http://localhost/perpustakaan2/petugas/peminjaman/';
		$config['per_page'] = 5;
		$config['num_links'] = 5;

		// styling
		$config['full_tag_open'] = '<nav><ul class="pagination justify-content-center">';
		$config['full_tag_close'] = '</ul></nav>';

		$config['first_link'] = 'Pertama';
		$config['first_tag_open'] = '<li class="page-item">';
		$config['first_tag_close'] = '</li>';

		$config['last_link'] = 'Terakhir';
		$config['last_tag_open'] = '<li class="page-item">';
		$config['last_tag_close'] = '</li>';

		$config['next_link'] = 'Lanjut';
		$config['next_tag_open'] = '<li class="page-item">';
		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = 'Kembali';
		$config['prev_tag_open'] = '<li class="page-item">';
		$config['prev_tag_close'] = '</li>';

		// $config['ref_link'] = 'Refresh';
		// $config['ref_tag_open'] = '<li class="page-item">';
		// $config['ref_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li class="page-item">';
		$config['num_tag_close'] = '</li>';

		$config['attributes'] = array('class' => 'page-link');

		// Initialize (konfirmasi)
		$this->pagination->initialize($config);

		// Get user dan start
		$data['startpeminjam'] = $this->uri->segment(3);
		$data['dataPeminjam'] = $this->getci->getPeminjam($config['per_page'], $data['startpeminjam'], $data['keyword_user'], true);
		// $data['dataPeminjamV2'] = $this->getci->getPeminjamV2();

		$data['setDataPeminjam'] = [
			'PETUGAS_USER' => 'petugas',
			'ADMIN_USER' => 'admin',
			'ANGGOTA_USER' => 'anggota'
		];

		$data['newMsg'] = $this->getci->getMessages();
		$data['newChat'] = $this->getci->getChat();
		$data['amountMsg'] = $this->getci->getAmountMessages();

		$data['setDataCom'] = [
			'PETUGAS_USER' => 'petugas',
			'ADMIN_USER' => 'admin',
			'ANGGOTA_USER' => 'anggota'
		];

		$data['bgColor'] = '#32325d';

		$this->setTemplates('user', 'dataPeminjaman', $data);
	}

	public function pengembalian()
	{
		$data['dashboardaktif'] = '';
		$data['adminaktif'] = '';
		$data['adminaktif1'] = '';
		$data['adminaktif2'] = '';
		$data['adminshow'] = '';
		$data['anggotaaktif'] = '';
		$data['anggotaaktif1'] = '';
		$data['anggotaaktif2'] = '';
		$data['anggotashow'] = '';
		$data['bukuaktif'] = '';
		$data['bukuaktif1'] = '';
		$data['bukuaktif2'] = '';
		$data['bukushow'] = '';
		$data['transaksiaktif'] = 'active';
		$data['transaksiaktif1'] = '';
		$data['transaksiaktif2'] = 'active';
		$data['transaksiaktif3'] = '';
		$data['transaksishow'] = 'show';
		$data['profilaktif'] = '';
		$data['profilaktif1'] = '';
		$data['profilaktif2'] = '';
		$data['profilshow'] = '';
		$data['communityaktif'] = '';

		$data['user'] = $this->db->get_where('petugas', ['fullname' => $this->session->userdata('fullname')])->row_array();
		$data['title'] = 'Data Pengembalian | Perpustakaan SMKN2 Banjarmasin';
		$data['sessionUser'] = $this->sessionci->setSession('petugas');
		// $this->sessionci->setUserdataBuku($data, '');

		// Save data search keyword
		if ($this->input->post('submit')) {
			$data['keyword_user'] = $this->input->post('keyword_user');
			$this->session->set_userdata('keyword_user', $data['keyword_user']);
		} else {
			$data['keyword_user'] = $this->session->userdata('keyword_user');
		}

		// Pagination config
		$this->db->like('nama_anggota', $data['keyword_user']);
		$this->db->or_like('judul_buku', $data['keyword_user']);
		$this->db->from('view_pengembalian2');

		// $config['total_rows'] = $this->getci->getAmountBukuById();
		$config['total_rows'] = $this->db->count_all_results();
		$data['total_rows_pengembalian'] = $config['total_rows'];
		$config['base_url'] = 'http://localhost/perpustakaan2/petugas/pengembalian/';
		$config['per_page'] = 5;
		$config['num_links'] = 5;

		// styling
		$config['full_tag_open'] = '<nav><ul class="pagination justify-content-center">';
		$config['full_tag_close'] = '</ul></nav>';

		$config['first_link'] = 'Pertama';
		$config['first_tag_open'] = '<li class="page-item">';
		$config['first_tag_close'] = '</li>';

		$config['last_link'] = 'Terakhir';
		$config['last_tag_open'] = '<li class="page-item">';
		$config['last_tag_close'] = '</li>';

		$config['next_link'] = 'Lanjut';
		$config['next_tag_open'] = '<li class="page-item">';
		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = 'Kembali';
		$config['prev_tag_open'] = '<li class="page-item">';
		$config['prev_tag_close'] = '</li>';

		// $config['ref_link'] = 'Refresh';
		// $config['ref_tag_open'] = '<li class="page-item">';
		// $config['ref_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li class="page-item">';
		$config['num_tag_close'] = '</li>';

		$config['attributes'] = array('class' => 'page-link');

		// Initialize (konfirmasi)
		$this->pagination->initialize($config);

		// Get user dan start
		$data['startpengembalian'] = $this->uri->segment(3);
		$data['dataPengembalian'] = $this->getci->getPengembalian($config['per_page'], $data['startpengembalian'], $data['keyword_user'], true);
		// $data['dataPeminjamV2'] = $this->getci->getPeminjamV2();

		$data['setDataPeminjam'] = [
			'PETUGAS_USER' => 'petugas',
			'ADMIN_USER' => 'admin',
			'ANGGOTA_USER' => 'anggota'
		];

		$data['newMsg'] = $this->getci->getMessages();
		$data['newChat'] = $this->getci->getChat();
		$data['amountMsg'] = $this->getci->getAmountMessages();

		$data['setDataCom'] = [
			'PETUGAS_USER' => 'petugas',
			'ADMIN_USER' => 'admin',
			'ANGGOTA_USER' => 'anggota'
		];

		$data['bgColor'] = '#32325d';

		$this->setTemplates('user', 'dataPengembalian', $data);
	}

	public function tambahPeminjaman()
	{
		$data['dashboardaktif'] = '';
		$data['adminaktif'] = '';
		$data['adminaktif1'] = '';
		$data['adminaktif2'] = '';
		$data['adminshow'] = '';
		$data['anggotaaktif'] = '';
		$data['anggotaaktif1'] = '';
		$data['anggotaaktif2'] = '';
		$data['anggotashow'] = '';
		$data['bukuaktif'] = '';
		$data['bukuaktif1'] = '';
		$data['bukuaktif2'] = '';
		$data['bukushow'] = '';
		$data['transaksiaktif'] = 'active';
		$data['transaksiaktif1'] = '';
		$data['transaksiaktif2'] = '';
		$data['transaksiaktif3'] = 'active';
		$data['transaksishow'] = 'show';
		$data['profilaktif'] = '';
		$data['profilaktif1'] = '';
		$data['profilaktif2'] = '';
		$data['profilshow'] = '';
		$data['communityaktif'] = '';


		$data['newMsg'] = $this->getci->getMessages();
		$data['newChat'] = $this->getci->getChat();
		$data['amountMsg'] = $this->getci->getAmountMessages();

		$data['setDataCom'] = [
			'PETUGAS_USER' => 'petugas',
			'ADMIN_USER' => 'admin',
			'ANGGOTA_USER' => 'anggota'
		];

		$data['user'] = $this->db->get_where('petugas', ['fullname' => $this->session->userdata('fullname')])->row_array();
		$data['listbuku'] = $this->getci->getAllBuku();
		// $data['listadmin'] = $this->getci->getAllAdmin();
		$data['title'] = 'Input Transaksi | Perpustakaan SMKN2 Banjarmasin';
		$data['listuser'] = $this->getci->getAllUser();
		$data['sessionUser'] = $this->sessionci->setSession('petugas');

		$data['bgColor'] = '#32325d';

		$this->form_validation->set_rules('anggota', 'Anggota', 'required|trim');
		$this->form_validation->set_rules('judul', 'Judul Buku', 'required|trim');
		$this->form_validation->set_rules('wk1', 'Waktu Peminjaman', 'required|trim');
		$this->form_validation->set_rules('wk2', 'Waktu Pengembelian', 'required|trim');

		if ($this->form_validation->run() == false) {
			$this->setTemplates('user', 'tambahPeminjaman', $data);
		} else {
			$this->addci->inputTransaksiV2($data['user']['id_petugas'], 'petugas');
			$this->session->set_flashdata('flash', 'diajukan');
			redirect('petugas/peminjaman');
		}
	}

	public function editPeminjaman($id)
	{
		$data['dashboardaktif'] = '';
		$data['adminaktif'] = '';
		$data['adminaktif1'] = '';
		$data['adminaktif2'] = '';
		$data['adminshow'] = '';
		$data['anggotaaktif'] = '';
		$data['anggotaaktif1'] = '';
		$data['anggotaaktif2'] = '';
		$data['anggotashow'] = '';
		$data['bukuaktif'] = '';
		$data['bukuaktif1'] = '';
		$data['bukuaktif2'] = '';
		$data['bukushow'] = '';
		$data['transaksiaktif'] = 'active';
		$data['transaksiaktif1'] = 'active';
		$data['transaksiaktif2'] = '';
		$data['transaksiaktif3'] = '';
		$data['transaksishow'] = 'show';
		$data['profilaktif'] = '';
		$data['profilaktif1'] = '';
		$data['profilaktif2'] = '';
		$data['profilshow'] = '';
		$data['communityaktif'] = '';

		$data['user'] = $this->db->get_where('petugas', ['fullname' => $this->session->userdata('fullname')])->row_array();
		$data['title'] = 'Edit Peminjaman | Perpustakaan SMKN2 Banjarmasin';
		$data['sessionUser'] = $this->sessionci->setSession('petugas');
		$data['listbuku'] = $this->getci->getAllBuku();
		$data['peminjamans'] = $this->getci->getPeminjamanById($id);

		$data['newMsg'] = $this->getci->getMessages();
		$data['amountMsg'] = $this->getci->getAmountMessages();

		$data['setDataCom'] = [
			'PETUGAS_USER' => 'petugas',
			'ADMIN_USER' => 'admin',
			'ANGGOTA_USER' => 'anggota'
		];

		$data['bgColor'] = '#32325d';

		$this->form_validation->set_rules('anggota', 'Anggota', 'required|trim');
		$this->form_validation->set_rules('judul', 'Judul Buku', 'required|trim');
		$this->form_validation->set_rules('wk1', 'Waktu Peminjaman', 'required|trim');
		$this->form_validation->set_rules('wk2', 'Waktu Pengembelian', 'required|trim');

		if ($this->form_validation->run() == false) {
			$this->setTemplates('user', 'editPeminjaman', $data);
		} else {
			$this->editci->editPeminjaman($data['user']['nama_petugas'] . ' (petugas)');
			$this->session->set_flashdata('flash', 'diedit');
			redirect('petugas/peminjaman');
		}
	}

	public function editPengembalian($id)
	{
		$data['dashboardaktif'] = '';
		$data['adminaktif'] = '';
		$data['adminaktif1'] = '';
		$data['adminaktif2'] = '';
		$data['adminshow'] = '';
		$data['anggotaaktif'] = '';
		$data['anggotaaktif1'] = '';
		$data['anggotaaktif2'] = '';
		$data['anggotashow'] = '';
		$data['bukuaktif'] = '';
		$data['bukuaktif1'] = '';
		$data['bukuaktif2'] = '';
		$data['bukushow'] = '';
		$data['transaksiaktif'] = 'active';
		$data['transaksiaktif1'] = '';
		$data['transaksiaktif2'] = 'active';
		$data['transaksiaktif3'] = '';
		$data['transaksishow'] = 'show';
		$data['profilaktif'] = '';
		$data['profilaktif1'] = '';
		$data['profilaktif2'] = '';
		$data['profilshow'] = '';
		$data['communityaktif'] = '';

		$data['user'] = $this->db->get_where('petugas', ['fullname' => $this->session->userdata('fullname')])->row_array();
		$data['title'] = 'Edit Pengembalian | Perpustakaan SMKN2 Banjarmasin';
		$data['sessionUser'] = $this->sessionci->setSession('petugas');
		$data['listbuku'] = $this->getci->getAllBuku();
		$data['peminjamans'] = $this->getci->getPengembalianById($id);

		$data['newMsg'] = $this->getci->getMessages();
		$data['amountMsg'] = $this->getci->getAmountMessages();

		$data['setDataCom'] = [
			'PETUGAS_USER' => 'petugas',
			'ADMIN_USER' => 'admin',
			'ANGGOTA_USER' => 'anggota'
		];

		$data['bgColor'] = '#32325d';

		$this->form_validation->set_rules('anggota', 'Anggota', 'required|trim');
		$this->form_validation->set_rules('judul', 'Judul Buku', 'required|trim');
		$this->form_validation->set_rules('wk2', 'Waktu Pengembelian', 'required|trim');

		if ($this->form_validation->run() == false) {
			$this->setTemplates('user', 'editPengembalian', $data);
		} else {
			$this->editci->editPengembalian($data['user']['nama_petugas'] . ' (petugas)');
			$this->session->set_flashdata('flash', 'diedit');
			redirect('petugas/pengembalian');
		}
	}

	public function ajukanPengembalian($id)
	{
		$data['dashboardaktif'] = '';
		$data['adminaktif'] = '';
		$data['adminaktif1'] = '';
		$data['adminaktif2'] = '';
		$data['adminshow'] = '';
		$data['anggotaaktif'] = '';
		$data['anggotaaktif1'] = '';
		$data['anggotaaktif2'] = '';
		$data['anggotashow'] = '';
		$data['bukuaktif'] = '';
		$data['bukuaktif1'] = '';
		$data['bukuaktif2'] = '';
		$data['bukushow'] = '';
		$data['transaksiaktif'] = 'active';
		$data['transaksiaktif1'] = '';
		$data['transaksiaktif2'] = '';
		$data['transaksiaktif3'] = 'active';
		$data['transaksishow'] = 'show';
		$data['profilaktif'] = '';
		$data['profilaktif1'] = '';
		$data['profilaktif2'] = '';
		$data['profilshow'] = '';
		$data['communityaktif'] = '';

		$data['user'] = $this->db->get_where('petugas', ['fullname' => $this->session->userdata('fullname')])->row_array();
		$data['title'] = 'Edit Peminjaman | Perpustakaan SMKN2 Banjarmasin';
		$data['sessionUser'] = $this->sessionci->setSession('petugas');
		$data['listbuku'] = $this->getci->getAllBuku();
		$data['peminjamans'] = $this->getci->getPeminjamanById($id);

		$data['newMsg'] = $this->getci->getMessages();
		$data['amountMsg'] = $this->getci->getAmountMessages();

		$data['setDataCom'] = [
			'PETUGAS_USER' => 'petugas',
			'ADMIN_USER' => 'admin',
			'ANGGOTA_USER' => 'anggota'
		];

		$data['bgColor'] = '#32325d';

		$this->form_validation->set_rules('anggota', 'Anggota', 'required|trim');
		$this->form_validation->set_rules('judul', 'Judul Buku', 'required|trim');
		$this->form_validation->set_rules('wk1', 'Waktu Peminjaman', 'required|trim');
		$this->form_validation->set_rules('wk2', 'Waktu Pengembelian', 'required|trim');
		$this->form_validation->set_rules('denda', 'Denda', 'required|trim');

		if ($this->form_validation->run() == false) {
			$this->setTemplates('user', 'ajukanPengembalian', $data);
			// var_dump($data['peminjamans']['id_peminjaman']);
		} else {
			// $this->editci->inputPengembalian();
			$ids = $data['peminjamans']['id_peminjaman'];
			// $this->delci->hapusPeminjaman($ids);
			$this->addci->inputPengembalianV2($data['user']['id_petugas'], $ids, 'petugas');
			// $this->session->set_flashdata('flash', 'dihapus');
			// redirect('petugas/peminjaman');
			$this->session->set_flashdata('flash', 'diajukan');
			redirect('petugas/pengembalian');
		}
	}

	public function hapusPeminjaman($id)
	{
		$this->delci->hapusPeminjaman($id);
		$this->session->set_flashdata('flash', 'dihapus');
		redirect('petugas/peminjaman');
	}

	public function hapusPengembalian($id)
	{
		$this->delci->hapusPengembalian($id);
		$this->session->set_flashdata('flash', 'dihapus');
		redirect('petugas/pengembalian');
	}
}

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Admin_model');
	}

	public function index()
	{
		$this->form_validation->set_rules('username', 'Username', 'required|trim');
		$this->form_validation->set_rules('password', 'Password', 'required|trim');
		if ($this->form_validation->run() == false) {
			$data['title'] = 'Login | Perpustakaan SMKN 2 Banjarmasin';
			$this->load->view('templates/auth_header', $data);
			$this->load->view('auth/login');
			$this->load->view('templates/auth_footer');
		} else {
			$this->_login();
		}
	}

	private function _login()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$petugas = $this->db->get_where('petugas', ['username' => $username])->row_array();
		$admin = $this->db->get_where('admin', ['username' => $username])->row_array();
		$anggota = $this->db->get_where('anggota', ['username' => $username])->row_array();

		if ($petugas) {
			// if (password_verify($password, $petugas['password'])) {
			if (check_password($password, $petugas['password'])) {
				$data = [
					'fullname' => $petugas['fullname']
				];
				$this->session->set_userdata($data);
				redirect('petugas');
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible fade show" role="alert">Wrong password<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
				redirect('auth');
			}
		} else if ($admin) {
			// if (password_verify($password, $admin['password'])) {
			if (check_password($password, $admin['password'])) {
				$data = [
					'fullname' => $admin['fullname']
				];
				$this->session->set_userdata($data);
				redirect('admin');
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible fade show" role="alert">Wrong password<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
				redirect('auth');
			}
		} else if ($anggota) {
			// if (password_verify($password, $anggota['password'])) {
			if (check_password($password, $anggota['password'])) {
				$data = [
					'fullname' => $anggota['fullname']
				];
				$this->session->set_userdata($data);
				redirect('anggota');
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible fade show" role="alert">Wrong password<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
				redirect('auth');
			}
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible fade show" role="alert">Username is not registered.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
			redirect('auth');
		}
	}

	public function registration()
	{

		$this->form_validation->set_rules('fullname', 'Fullname', 'required|trim|min_length[4]|max_length[20]', [
			'min_length' => 'Fullname too short!',
			'max_length' => 'Fullname too long!'
		]);
		$this->form_validation->set_rules('username', 'Username', 'required|trim|min_length[6]|max_length[20]', [
			'min_length' => 'Username too short!',
			'max_length' => 'Username too long!'
		]);
		$this->form_validation->set_rules('password1', 'Password', 'required|trim|min_length[4]|max_length[20]|matches[password2]', [
			'matches' => 'Password dont match!',
			'min_length' => 'Password too short!',
			'max_length' => 'Password too long!'
		]);
		$this->form_validation->set_rules('password2', 'Password', 'required|trim|matches[password1]');
		if ($this->form_validation->run() == false) {
			$data['title'] = 'Registration';
			$this->load->view('templates/auth_header', $data);
			$this->load->view('auth/registration');
			$this->load->view('templates/auth_footer');
		} else {
			$data = [
				'username' => htmlspecialchars($this->input->post('username', true)),
				'password' => password_encryption($this->input->post('password1')),
				// 'password' => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
				'fullname' => htmlspecialchars($this->input->post('fullname', true)),
				'nama_admin' => htmlspecialchars($this->input->post('fullname', true)),
				'color' => htmlspecialchars('dark')
			];

			$this->db->insert('admin', $data);
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">Congratulation! your account has been created. Please Login<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
			redirect('auth');
		}
	}


	public function registration2()
	{
		$this->form_validation->set_rules('fullname', 'Fullname', 'required|trim|min_length[4]|max_length[20]', [
			'min_length' => 'Fullname too short!',
			'max_length' => 'Fullname too long!'
		]);
		$this->form_validation->set_rules('username', 'Username', 'required|trim|min_length[6]|max_length[20]', [
			'min_length' => 'Username too short!',
			'max_length' => 'Username too long!'
		]);
		$this->form_validation->set_rules('password1', 'Password', 'required|trim|min_length[4]|max_length[20]|matches[password2]', [
			'matches' => 'Password dont match!',
			'min_length' => 'Password too short!',
			'max_length' => 'Password too long!'
		]);
		$this->form_validation->set_rules('password2', 'Password', 'required|trim|matches[password1]');
		if ($this->form_validation->run() == false) {
			$data['title'] = 'Registration';
			$this->load->view('templates/auth_header', $data);
			$this->load->view('auth/registration2');
			$this->load->view('templates/auth_footer');
		} else {
			$data = [
				'username' => htmlspecialchars($this->input->post('username', true)),
				'password' => password_encryption($this->input->post('password1')),
				// 'password' => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
				'fullname' => htmlspecialchars($this->input->post('fullname', true)),
				'nama_petugas' => htmlspecialchars($this->input->post('fullname', true)),
				'color' => htmlspecialchars('dark')
			];

			$this->db->insert('petugas', $data);
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">Congratulation! your account has been created. Please Login<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
			redirect('auth');
		}
	}

	public function logout()
	{
		$this->session->unset_userdata('fullname');
		$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">You have been logged out!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
		redirect('auth');
	}

	public function blocked()
	{
		$this->session->unset_userdata('fullname');
		$this->load->view('auth/blocked');
	}
}

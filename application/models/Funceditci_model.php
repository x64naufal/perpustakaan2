<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Funceditci_model extends CI_model
{
    public function editAnggota()
    {
        $data = [
            'id_anggota' => $this->input->post('id_anggota', true),
            'username' => htmlspecialchars($this->input->post('username', true)),
            'fullname' => htmlspecialchars($this->input->post('nama', true)),
            'nama_anggota' => htmlspecialchars($this->input->post('nama', true)),
            'jk_anggota' => htmlspecialchars($this->input->post('jk', true)),
            'kelas_anggota' => htmlspecialchars($this->input->post('kelas', true)),
            'jurusan_anggota' => htmlspecialchars($this->input->post('jurusan')),
            'no_telp_anggota' => $this->input->post('no_telp', true),
            'alamat_anggota' => htmlspecialchars($this->input->post('alamat', true))
        ];

        $this->db->where('id_anggota', $this->input->post('id_anggota'));
        $this->db->update('anggota', $data);
    }

    public function editBackground()
    {
        $data = [
            'id_petugas' => $this->input->post('id_petugas', true),
            'color' => htmlspecialchars($this->input->post('color', true))

        ];

        $this->db->where('id_petugas', $this->input->post('id_petugas'));
        $this->db->update('petugas', $data);
    }

    public function editBuku()
    {
        $data = [
            'id_buku' => $this->input->post('id_buku', true),
            'judul_buku' => htmlspecialchars($this->input->post('judul', true)),
            'genre_buku' => htmlspecialchars($this->input->post('genre', true)),
            'penulis_buku' => htmlspecialchars($this->input->post('penulis', true)),
            'penerbit_buku' => htmlspecialchars($this->input->post('penerbit')),
            'tahun_penerbit' => $this->input->post('tahun', true),
            'stok_buku' => htmlspecialchars($this->input->post('stok', true))
        ];

        $this->db->where('id_buku', $this->input->post('id_buku'));
        $this->db->update('buku', $data);
    }

    public function editAdmin()
    {
        $data = [
            'id_admin' => $this->input->post('id_admin', true),
            'username' => htmlspecialchars($this->input->post('username', true)),
            'fullname' => htmlspecialchars($this->input->post('nama', true)),
            'nama_admin' => htmlspecialchars($this->input->post('nama', true))

        ];

        $this->db->where('id_admin', $this->input->post('id_admin'));
        $this->db->update('admin', $data);
    }

    public function editPeminjaman($id)
    {
        $data = [
            'id_peminjaman' => $this->input->post('id_peminjaman', true),
            'id_buku' => htmlspecialchars($this->input->post('judul', true)),
            'nama_petugas' => htmlspecialchars($id),
            'tanggal_pinjam' => htmlspecialchars($this->input->post('wk1', true)),
            'tanggal_kembali' => htmlspecialchars($this->input->post('wk2', true)),
            'status' => htmlspecialchars($this->input->post('status', true))

        ];

        $this->db->where('id_peminjaman', $this->input->post('id_peminjaman'));
        $this->db->update('peminjaman', $data);
    }

    public function editPengembalian($id)
    {
        $data = [
            'id_pengembalian' => $this->input->post('id_pengembalian', true),
            'id_buku' => htmlspecialchars($this->input->post('judul', true)),
            'nama_petugas' => htmlspecialchars($id),
            // 'tanggal_pinjam' => htmlspecialchars($this->input->post('wk1', true)),
            'tanggal_pengembalian' => htmlspecialchars($this->input->post('wk2', true)),
            'status' => htmlspecialchars($this->input->post('status', true))

        ];

        $this->db->where('id_pengembalian', $this->input->post('id_pengembalian'));
        $this->db->update('pengembalian', $data);
    }
}

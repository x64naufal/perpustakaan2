<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Funcaddci_model extends CI_model
{
    public function addLiveChat($user, $session)
    {
        $data = getdate();
        $data = [
            'chat_user' => htmlspecialchars($user),
            'chat_session' => htmlspecialchars($session),
            'chat_date' => htmlspecialchars($data['month'] . ' ' . $data['mday'] . ', ' . $data['year']),
            'chat_msg' => htmlspecialchars($this->input->post('msg', true))
        ];

        $this->db->insert('livechat', $data);
    }

    public function addAdmin()
    {
        $data = [
            'username' => htmlspecialchars($this->input->post('username', true)),
            'password' => password_encryption($this->input->post('password')),
            // 'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
            'fullname' => htmlspecialchars($this->input->post('nama', true)),
            'nama_admin' => htmlspecialchars($this->input->post('nama', true))
        ];

        $this->db->insert('admin', $data);
    }

    public function addAnggota()
    {
        $data = [
            'username' => htmlspecialchars($this->input->post('username', true)),
            'password' => password_encryption($this->input->post('password')),
            // 'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
            'fullname' => htmlspecialchars($this->input->post('nama', true)),
            'nama_anggota' => htmlspecialchars($this->input->post('nama', true)),
            'color' => htmlspecialchars('dark'),
            'jk_anggota' => htmlspecialchars($this->input->post('jk', true)),
            'kelas_anggota' => htmlspecialchars($this->input->post('kelas', true)),
            'jurusan_anggota' => htmlspecialchars($this->input->post('jurusan', true)),
            'no_telp_anggota' => $this->input->post('no_telp', true),
            'alamat_anggota' => htmlspecialchars($this->input->post('alamat', true))
        ];

        $this->db->insert('anggota', $data);
    }

    public function addBuku()
    {
        $data = [
            'judul_buku' => htmlspecialchars($this->input->post('judul', true)),
            'genre_buku' => htmlspecialchars($this->input->post('genre', true)),
            'penulis_buku' => htmlspecialchars($this->input->post('penulis', true)),
            'penerbit_buku' => htmlspecialchars($this->input->post('penerbit')),
            'tahun_penerbit' => $this->input->post('tahun', true),
            'stok_buku' => htmlspecialchars($this->input->post('stok', true))
        ];

        $this->db->insert('buku', $data);
    }

    public function inputTransaksi($aid)
    {
        $tnow = $this->getci->getTimeByStr('now');
        // $tk = $this->input->post('wk', true);
        // switch ($tk) {
        //     case '3':
        //         $tlas =  $this->getci->getTimeByStr('3 days');
        //         break;
        //     case '7':
        //         $tlas =  $this->getci->getTimeByStr('7 days');
        //         break;
        // }
        $data2 = [
            'tanggal_pinjam' => htmlspecialchars($tnow),
            // 'tanggal_kembali' => htmlspecialchars($tlas),
            'tanggal_kembali' => htmlspecialchars($this->input->post('wk', true)),
            'id_buku' => htmlspecialchars($this->input->post('judul', true)),
            'id_anggota' => htmlspecialchars($aid),
            'id_petugas' =>  htmlspecialchars($this->input->post('petugas', true))
        ];
        $this->db->insert('peminjaman', $data2);
    }

    public function inputTransaksiV2($aid, $role = null)
    {
        // $tnow = $this->getci->getTimeByStr('now');
        switch ($role) {
            case 'petugas' && 'admin':
                $data2 = [
                    'tanggal_pinjam' => htmlspecialchars($this->input->post('wk1', true)),
                    'tanggal_kembali' => htmlspecialchars($this->input->post('wk2', true)),
                    'id_buku' => htmlspecialchars($this->input->post('judul', true)),
                    'id_anggota' => htmlspecialchars($this->input->post('anggota', true)),
                    'id_petugas' =>  htmlspecialchars($aid),
                    'status' =>  htmlspecialchars("tunda"),
                ];
                $this->db->insert('peminjaman', $data2);
                break;
            case 'anggota':
                $data2 = [
                    'tanggal_pinjam' => htmlspecialchars($this->input->post('wk1', true)),
                    'tanggal_kembali' => htmlspecialchars($this->input->post('wk2', true)),
                    'id_buku' => htmlspecialchars($this->input->post('judul', true)),
                    'id_anggota' => htmlspecialchars($this->input->post('anggota', true)),
                    'id_petugas' =>  htmlspecialchars($aid),
                    'status' =>  htmlspecialchars("tunda"),
                ];
                $this->db->insert('peminjaman', $data2);
                break;
        }
    }

    public function inputPengembalianV2($aid, $aud, $role = null)
    {
        // $tnow = $this->getci->getTimeByStr('now');
        if ($this->input->post('denda', true) == '') {
            $denda = '0';
        } else {
            $denda = $this->input->post('denda', true);
        }
        switch ($role) {
            case 'petugas' && 'admin':
                $data2 = [
                    'tanggal_pengembalian' => htmlspecialchars($this->input->post('wk2', true)),
                    'denda' =>  htmlspecialchars($denda),
                    'id_buku' => htmlspecialchars($this->input->post('judul', true)),
                    'id_anggota' => htmlspecialchars($this->input->post('anggota', true)),
                    'id_petugas' =>  htmlspecialchars($aid),
                    'status' =>  htmlspecialchars("tunda"),
                ];
                $this->db->insert('pengembalian', $data2);

                break;
            case 'anggota':
                $data2 = [
                    'tanggal_pengembalian' => htmlspecialchars($this->input->post('wk2', true)),
                    'denda' =>  htmlspecialchars($denda),
                    'id_buku' => htmlspecialchars($this->input->post('judul', true)),
                    'id_anggota' => htmlspecialchars($aud),
                    // 'id_petugas' =>  htmlspecialchars($aid),
                    'status' =>  htmlspecialchars("tunda"),
                ];
                $this->db->insert('pengembalian', $data2);
                break;
        }
    }
}

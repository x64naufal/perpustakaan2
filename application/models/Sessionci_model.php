<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sessionci_model extends CI_model
{
    public function setSession($session = null)
    {
        switch ($session) {
            case 'petugas':
                return 'PETUGAS_USER';
                break;
            case 'admin':
                return 'ADMIN_USER';
                break;
            case 'anggota':
                return 'ANGGOTA_USER';
                break;
        }
    }

    public function setUserdataBuku($datas, $isi)
    {
        $datas['keyword_buku'] = $isi;
        $this->session->set_userdata('keyword_buku', $datas['keyword_buku']);
    }
}

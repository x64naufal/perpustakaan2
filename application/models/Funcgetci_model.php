<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Funcgetci_model extends CI_model
{

    public function getColor($user)
    {
        //$color = $this->db->get_where($user, ['color' => $id])->row_array();
        switch ($user) {
            case 'light':
                return '#adb5bd'; //lightmode background
                break;
            case 'dark':
                return '#32325d'; //darkmoide background
                break;
        }
    }

    public function getTimeByStr($tm)
    {
        $d = strtotime($tm);
        return date("Y-m-d", $d);
    }

    public function getAmountMessages()
    {
        $query = $this->db->get('announce');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }

    public function getAmountAdminById()
    {
        $query = $this->db->get('admin');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }

    public function getAmountAnggotaById()
    {
        $query = $this->db->get('anggota');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }

    public function getAmountBukuById()
    {
        $query = $this->db->get('buku');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }

    public function getMessages()
    {
        return $this->db->get('announce')->result_array();
    }

    public function getChat()
    {
        return $this->db->get('livechat')->result_array();
    }

    public function getAllAdmin()
    {
        return $this->db->get('admin')->result_array();
    }

    public function getAllUser()
    {
        return $this->db->get('anggota')->result_array();
    }

    public function getAllBuku()
    {
        return $this->db->get('buku')->result_array();
    }

    public function getPeminjam($limit, $start, $keyword = null)
    {
        if ($keyword) {
            $this->db->like('nama_anggota', $keyword);
            $this->db->or_like('judul_buku', $keyword);
        }
        return $this->db->get('view_peminjaman2', $limit, $start)->result_array();
        // return $this->db->get('peminjaman', $limit, $start)->result_array();
    }

    public function getPengembalian($limit, $start, $keyword = null)
    {
        if ($keyword) {
            $this->db->like('nama_anggota', $keyword);
            $this->db->or_like('judul_buku', $keyword);
        }
        return $this->db->get('view_pengembalian2', $limit, $start)->result_array();
        // $this->db->select('*');
        // $this->db->from('pengembalian');
        // $this->db->join('anggota', 'anggota.id_anggota = pengembalian.id_anggota');
        // $this->db->join('buku', 'buku.id_buku = pengembalian.id_buku');
        // $this->db->join('admin', 'admin.id_admin = pengembalian.id_petugas');

        // return $this->db->get($limit, $start)->result_array();
        // return $this->db->get('peminjaman', $limit, $start)->result_array();
    }

    public function getPeminjamV2()
    {
        $this->db->select('*');
        $this->db->from('peminjaman');
        $this->db->join('anggota', 'anggota.id_anggota = peminjaman.id_anggota');
        $this->db->join('buku', 'buku.id_buku = peminjaman.id_buku');
        $this->db->join('admin', 'admin.id_admin = peminjaman.id_petugas');

        return $this->db->get()->result_array();
        // return $this->db->get('view_peminjaman')->result_array();
    }

    public function getAdmin($limit, $start, $keyword = null)
    {
        if ($keyword) {
            $this->db->like('fullname', $keyword);
            $this->db->or_like('username', $keyword);
        }
        return $this->db->get('admin', $limit, $start)->result_array();
    }

    public function getAnggota($limit, $start, $keyword = null)
    {
        if ($keyword) {
            $this->db->like('fullname', $keyword);
            $this->db->or_like('username', $keyword);
        }
        return $this->db->get('anggota', $limit, $start)->result_array();
    }

    public function getBuku($limit, $start, $keyword = null,  $multiple = null)
    {
        if ($keyword) {
            $this->db->like('judul_buku', $keyword);
            if ($multiple == true) {
                $this->db->or_like('penulis_buku', $keyword);
            }
        }
        return $this->db->get('buku', $limit, $start)->result_array();
    }

    public function getPetugasById($id)
    {
        return $this->db->get_where('petugas', ['id_petugas' => $id])->row_array();
    }

    public function getAdminById($id)
    {
        return $this->db->get_where('admin', ['id_admin' => $id])->row_array();
    }

    public function getAnggotaById($id)
    {
        return $this->db->get_where('anggota', ['id_anggota' => $id])->row_array();
    }

    public function getBukuById($id)
    {
        return $this->db->get_where('buku', ['id_buku' => $id])->row_array();
    }

    public function getPeminjamanById($id)
    {
        return $this->db->get_where('view_peminjaman2', ['id_peminjaman' => $id])->row_array();
    }

    public function getPengembalianById($id)
    {
        return $this->db->get_where('view_pengembalian2', ['id_pengembalian' => $id])->row_array();
    }
}

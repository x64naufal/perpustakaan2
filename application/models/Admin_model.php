<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_model extends CI_model
{
	public function hapusAdmin($id)
	{
		$this->db->where('id_admin', $id);
		$this->db->delete('admin');
	}

	public function hapusAnggota($id)
	{
		$this->db->where('id_anggota', $id);
		$this->db->delete('anggota');
	}

	public function hapusBuku($id)
	{
		$this->db->where('id_buku', $id);
		$this->db->delete('buku');
	}
}

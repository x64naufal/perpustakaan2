<?php

// A simply cryptography for encryption user password
// TO DO : make a convert string to bytes array and re-back
// Author : Naufal Elghani (@naufalagler)

function checkIsString($str1, $str2) {
	if ($str1 == $str2) {
		return true;
	}
	return false;
}

function checkSizeString($str)
{
	if (strlen($str) <= 20) {
		return true;
	}
	return false;
}
function reverse($a, $b)
{
	return $b - $a;
}

function stringToBytesArray($str)
{
	$byte_array = unpack('C*', $str);
	return $byte_array;
}

// function bytesArrayToString($byte_array)
// {
// 	$str = call_user_func_array("pack", array_merge(array("C*"), $byte_array));
// 	return $str;
// }

function bytesArrayToString($byteArray)
{
	$chars = array_map("chr", $byteArray);
	return join($chars);
}

function byteArray2Hex($byteArray)
{
	$chars = array_map("chr", $byteArray);
	$bin = join($chars);
	return bin2hex($bin);
}

function hex2ByteArray($hexString)
{
	$string = hex2bin($hexString);
	return unpack('C*', $string);
}

function makeString($t1)
{
	if (checkSizeString($t1)) {
		$rv = strrev($t1);
		$sp = str_split($rv);
		$no = 0;
		$xyz = [];
		foreach ($sp as $p) {
			$k = reverse($no, strlen($t1));
			array_push($xyz, $p . $k);
			// $xyz .= $p . $k;
			$no += 1;
		}
		return $xyz;
	}
}

function remakeString($t1)
{
	// $rv = strrev($t1);
	$no = 0;
	$data = '';
	if(is_object($t1)) {
		$sp = str_split($t1);
		foreach ($sp as $p) {
		    $k = reverse($no, strlen($t1));
		    // $data .= '<br>' . $p;
		    // $data .= '<br>' . str_replace(substr($k, -1), '', $p);
		    $data .= '<br>' . str_replace($k, '', $p);
		    // $data .= '<br>' . $p;
		    $no += 1;
	    }
	} else if (is_array($t1)) {
		foreach ($t1 as $p) {
		    $k = reverse($no, count($t1));
		    // $data .= '<br>' . $p;
		    // $data .= '<br>' . str_replace(substr($k, -1), '', $p);
		    $data .= '<br>' . str_replace($k, '', $p);
		    // $data .= '<br>' . $p;
		    $no += 1;
	    }
	}
	return $data;
}

function remakeStringV2($t1) {
	$no = 0;
	$data = '';
	foreach ($sp as $p) {
		$k = reverse($no, strlen($t1));
		// $data .= '<br>' . $p;
		$data .= '<br>' . str_replace($no, '', $p);
		$no += 1;
	}
	return $data;
}

function password_encryption($text)
{
	$str = makeString($text);
	$no = 0;
	$tt = '';
	foreach($str as $c) {
		$k = reverse($no, count($str));
		$tt .= str_replace($k, '', $c);
		$no += 1;
	}
	$byte = stringToBytesArray($tt);
	return byteArray2Hex($byte);
}

function cpword($p1, $p2) {
	if ($p1 == $p2) {
		return true;
	}
	return false;
}

function check_passwordV2($str1, $str2)
{
	$hex = hex2ByteArray($str2);
	$byte = strrev(bytesArrayToString($hex));
	// $check = remakeString($byte);
	// if ($hex == $str2) {
	// 	echo $byte;
	// }

	// echo $byte;
	// if (strcmp($byte, $str1) == 0) {
	// 	return true;
	// }
	if ($byte == $str1) {
		return true;
	}
	return false;
}

function login()
{
	$user = [
		'pass' => 'naufal25', // default: naufal24
		'enc' => '34326c616675616e'
	];
	// $enc = password_encryption($user['pass']);
	if (check_passwordV2($user['pass'], $user['enc'])) {
		echo 'good';
	} else {
		echo 'false';
	}
}
